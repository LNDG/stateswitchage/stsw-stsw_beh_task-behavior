clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data         = fullfile(pn.root, 'data');
    pn.tools    = fullfile(pn.root, 'tools'); addpath(pn.tools);
    pn.bars     = fullfile(pn.root, 'tools', 'barwitherr'); addpath(pn.bars);
    pn.brewer   = fullfile(pn.root, 'tools', 'BrewerMap'); addpath(pn.brewer);
    pn.ploterr  = fullfile(pn.root, 'tools', 'ploterr'); addpath(pn.ploterr);
pn.plotFolder   = fullfile(pn.root, 'figures');

load(fullfile(pn.data, 'SummaryData_N102.mat'), 'SummaryData', 'IDs_all');

%SummaryData.EEG.RTs_mean % ID*Attribute*Dimension

filename = fullfile(pn.root, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

% select subjects (multimodal only)
idxMulti_summary = ismember(IDs_all, IDs_EEGMR);
[IDs_all(idxMulti_summary), IDs_EEGMR]

% define YA and OA
idx_YA = cellfun(@str2num, IDs_all)<2000;
idx_OA = cellfun(@str2num, IDs_all)>2000;

%% variability across features by age group

acc_merged = squeeze(cat(2, SummaryData.EEG.Acc_mean, SummaryData.MRI.Acc_mean));
%acc_merged = squeeze(cat(2, SummaryData.EEG.RTs_mean, SummaryData.MRI.RTs_mean));

dat_ya = squeeze(acc_merged(idxMulti_summary & idx_YA,:,1:4));
dat_oa = squeeze(acc_merged(idxMulti_summary & idx_OA,:,1:4));

x1 = squeeze(nanstd(dat_ya(:,:,1),[],2));
x2 = squeeze(nanstd(dat_oa(:,:,1),[],2));

y1 = squeeze(nanstd(dat_ya(:,:,4),[],2));
y2 = squeeze(nanstd(dat_oa(:,:,4),[],2));

h = figure('units','normalized','position',[.1 .1 .15 .2]);
    set(0, 'DefaultFigureRenderer', 'painters');
    hold on;
    bar(1-.2, nanmean(x1), 'FaceColor',  [1 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    bar(1+.2, nanmean(x2), 'FaceColor',  [.8 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    bar(2-.2, nanmean(y1), 'FaceColor',  [1 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    bar(2+.2, nanmean(y2), 'FaceColor',  [.8 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    % show standard error on top
    h1 = ploterr(1-.2, nanmean(x1,1), [], nanstd(x1,[],1)./sqrt(size(x1,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); set(h1(2), 'LineWidth', 4);
    h2 = ploterr(1+.2, nanmean(x2,1), [], nanstd(x2,[],1)./sqrt(size(x2,1)), 'k.', 'abshhxy', 0);
    set(h2(1), 'marker', 'none'); set(h2(2), 'LineWidth', 4);
    h1 = ploterr(2-.2, nanmean(y1,1), [], nanstd(y1,[],1)./sqrt(size(y1,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); set(h1(2), 'LineWidth', 4);
    h2 = ploterr(2+.2, nanmean(y2,1), [], nanstd(y2,[],1)./sqrt(size(y2,1)), 'k.', 'abshhxy', 0);
    set(h2(1), 'marker', 'none'); set(h2(2), 'LineWidth', 4);
    [h, pval] = ttest2(x1(:,1), x2(:,1)); mysigstar(gca, [1], .05, pval);
    [h, pval] = ttest2(y1(:,1), y2(:,1)); mysigstar(gca, [2], .05, pval);
    ylabel({'SD Accuracy';'(across features)'}); %xlabel('# of targets');
    legend({'YA'; 'OA'})
    set(gca, 'xtick', [1 2], 'xticklabel', {'L1'; 'L4'}, 'xlim', [0.5 2.5]);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)


	[h, pval] = ttest(x1(:,1), y1(:,1))
	[h, pval] = ttest(x2(:,1), y2(:,1))

%% select best individual L1 feature in each session, separately for EEG and fMRI

acc_merged = squeeze(cat(2, SummaryData.EEG.Acc_mean));%, SummaryData.MRI.Acc_mean));
rt_merged = squeeze(cat(2, SummaryData.EEG.RTs_mean));%, SummaryData.MRI.RTs_mean));

dat_ya = squeeze(acc_merged(idxMulti_summary & idx_YA,:,1:4));
dat_oa = squeeze(acc_merged(idxMulti_summary & idx_OA,:,1:4));
dat_ya_rt = squeeze(rt_merged(idxMulti_summary & idx_YA,:,1:4));
dat_oa_rt = squeeze(rt_merged(idxMulti_summary & idx_OA,:,1:4));

for i_sub = 1:size(dat_ya,1)
    [~, idx_feat_ya] = sort(dat_ya(i_sub,:,1), 'descend');
    data_ya_matched(1, i_sub,:) = squeeze(dat_ya(i_sub,idx_feat_ya(1),:));
    rt_ya_matched(1, i_sub,:) = dat_ya_rt(i_sub,idx_feat_ya(1),:);
    data_ya_unmatched(1, i_sub,:) = squeeze(dat_ya(i_sub,idx_feat_ya(2),:));
    rt_ya_unmatched(1, i_sub,:) = dat_ya_rt(i_sub,idx_feat_ya(2),:);
end

for i_sub = 1:size(dat_oa,1)
    [~, idx_feat_oa] = sort(dat_oa(i_sub,:,1), 'descend');
    data_oa_matched(1, i_sub,:) = dat_oa(i_sub,idx_feat_oa(1),:);
    rt_oa_matched(1, i_sub,:) = dat_oa_rt(i_sub,idx_feat_oa(1),:);
    data_oa_unmatched(1, i_sub,:) = dat_oa(i_sub,idx_feat_oa(2),:);
    rt_oa_unmatched(1, i_sub,:) = dat_oa_rt(i_sub,idx_feat_oa(2),:);
end

%% plot matched and unmatched data
% 
% % Note: these are standard errors, but for now includign al subs
% figure;
% hold on;
% error = squeeze(nanstd(nanmean(data_ya_matched(1,:,:),1),[],2))./sqrt(42);
% errorbar(squeeze(nanmean(nanmean(data_ya_matched(1,:,:),1),2)), error, 'k', ...
%     'linewidth', 2);
% error = squeeze(nanstd(nanmean(data_oa_matched(1,:,:),1),[],2))./sqrt(52);
% errorbar(squeeze(nanmean(nanmean(data_oa_matched(1,:,:),1),2)), error, 'r', ...
%     'linewidth', 2);
% set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
%         'xlim', [0.5 4.5]); ylim([0 1])
% ylabel('Mean Accuracy'); xlabel('# of targets');
% ylim([.6 1])
% set(findall(gcf,'-property','FontSize'),'FontSize',18)
% legend({'YA'; 'OA'})
% title("L1 acc matched")
% 
% figure;
% hold on;
% error = squeeze(mean(nanstd(rt_ya_matched(1,:,:),[],2),1))./sqrt(42);
% errorbar(squeeze(mean(nanmean(rt_ya_matched(1,:,:),2),1)), error, 'k', ...
%     'linewidth', 2);
% error = squeeze(mean(nanstd(rt_oa_matched(1,:,:),[],2),1))./sqrt(53);
% errorbar(squeeze(mean(nanmean(rt_oa_matched(1,:,:),2),1)), error, 'r', ...
%     'linewidth', 2);
% set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
%         'xlim', [0.5 4.5]); ylim([0 1])
% ylabel('Mean RT'); xlabel('# of targets');
% ylim([.4 1.1])
% set(findall(gcf,'-property','FontSize'),'FontSize',18)
% legend({'YA'; 'OA'})
% title("L1 acc matched")
% 
% figure;
% hold on;
% error = squeeze(nanstd(nanmean(data_ya_unmatched(1:3,:,:),1),[],2))./sqrt(42);
% errorbar(squeeze(nanmean(nanmean(data_ya_unmatched(1:3,:,:),1),2)), error, 'k', ...
%     'linewidth', 2);
% error = squeeze(nanstd(nanmean(data_oa_unmatched(1:3,:,:),1),[],2))./sqrt(52);
% errorbar(squeeze(nanmean(nanmean(data_oa_unmatched(1:3,:,:),1),2)), error, 'r', ...
%     'linewidth', 2);
% set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
%         'xlim', [0.5 4.5]); ylim([0 1])
% ylabel('Mean Accuracy'); xlabel('# of targets');
% ylim([.55 .95])
% set(findall(gcf,'-property','FontSize'),'FontSize',18)
% legend({'YA'; 'OA'})
% title("L1 acc unmatched")
% 
% figure;
% hold on;
% error = squeeze(mean(nanstd(rt_ya_unmatched(1:3,:,:),[],2),1))./sqrt(42);
% errorbar(squeeze(mean(nanmean(rt_ya_unmatched(1:3,:,:),2),1)), error, 'k', ...
%     'linewidth', 2);
% error = squeeze(mean(nanstd(rt_oa_unmatched(1:3,:,:),[],2),1))./sqrt(53);
% errorbar(squeeze(mean(nanmean(rt_oa_unmatched(1:3,:,:),2),1)), error, 'r', ...
%     'linewidth', 2);
% set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
%         'xlim', [0.5 4.5]); ylim([0 1])
% ylabel('Mean RT'); xlabel('# of targets');
% ylim([.35 1])
% set(findall(gcf,'-property','FontSize'),'FontSize',18)
% legend({'YA'; 'OA'})
% title("L1 acc unmatched")
% 
% % figure;
% % hold on;
% % plot(squeeze(mean(nanmean(rt_ya_matched(1:8,:,:),2),1)), 'k');
% % plot(squeeze(mean(nanmean(rt_oa_matched(1:8,:,:),2),1)), 'r');
% 
% [h, p] = ttest2(nanmean(data_ya_matched(:,:,1),1), nanmean(data_oa_matched(:,:,1),1))
% [h, p] = ttest2(nanmean(data_ya_matched(:,:,4),1), nanmean(data_oa_matched(:,:,4),1))
% [h, p] = ttest2(nanmean(rt_ya_matched(:,:,1),1), nanmean(rt_oa_matched(:,:,1),1))

%% add figures

% Note: these are standard errors, but for now includign all subs
figure;
subplot(1,2,1);
    hold on;
    error = squeeze(nanstd(nanmean(data_ya_matched(:,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(:,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(:,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(:,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_unmatched(:,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_unmatched(:,:,:),1),2)), error, 'k--', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_unmatched(:,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_unmatched(:,:,:),1),2)), error, 'r--', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]); ylim([0 1])
    ylabel('Mean Accuracy'); xlabel('# of targets');
    ylim([.5 1]);
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    legend({'YA top feature'; 'OA top feature'; 'YA worst'; 'OA worst'});
    %title("L1 acc matched")
subplot(1,2,2);
    hold on;
    error = squeeze(mean(nanstd(rt_ya_matched(:,:,:),[],2),1))./sqrt(42);
    errorbar(squeeze(mean(nanmean(rt_ya_matched(:,:,:),2),1)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(mean(nanstd(rt_oa_matched(:,:,:),[],2),1))./sqrt(53);
    errorbar(squeeze(mean(nanmean(rt_oa_matched(:,:,:),2),1)), error, 'r', ...
        'linewidth', 2);
    %
    error = squeeze(mean(nanstd(rt_ya_unmatched(:,:,:),[],2),1))./sqrt(42);
    errorbar(squeeze(mean(nanmean(rt_ya_unmatched(:,:,:),2),1)), error, 'k--', ...
        'linewidth', 2);
    error = squeeze(mean(nanstd(rt_oa_unmatched(:,:,:),[],2),1))./sqrt(53);
    errorbar(squeeze(mean(nanmean(rt_oa_unmatched(:,:,:),2),1)), error, 'r--', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]); ylim([0 1])
    ylabel('Mean RT'); xlabel('# of targets');
    ylim([.4 1.1]);
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    legend({'YA best feature'; 'OA best feature'; 'YA worst'; 'OA worst'}, ...
        'location', 'southeast');
    % title("L1 acc matched")

%% plot separately for YA and OA
    
% sort by feature performance in L1

for i_sub = 1:size(dat_ya,1)
    [~, idx_feat_ya] = sort(dat_ya(i_sub,:,1), 'descend');
    data_ya_matched(1:4, i_sub,:) = squeeze(dat_ya(i_sub,idx_feat_ya,:));
    rt_ya_matched(1:4, i_sub,:) = dat_ya_rt(i_sub,idx_feat_ya,:);
end

for i_sub = 1:size(dat_oa,1)
    [~, idx_feat_oa] = sort(dat_oa(i_sub,:,1), 'descend');
    data_oa_matched(1:4, i_sub,:) = dat_oa(i_sub,idx_feat_oa,:);
    rt_oa_matched(1:4, i_sub,:) = dat_oa_rt(i_sub,idx_feat_oa,:);
end

% Note: these are standard errors, but for now includign all subs
figure;
subplot(1,2,1);
    hold on;
    error = squeeze(nanstd(nanmean(data_ya_matched(1,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(1,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(2,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(2,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(3,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(3,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(4,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(4,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]); ylim([0 1])
    ylabel('Mean Accuracy'); xlabel('# of targets');
    ylim([.5 1]);
    title('YA')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
subplot(1,2,2);
    hold on;
    error = squeeze(nanstd(nanmean(data_oa_matched(1,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(2,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(2,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(3,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(3,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(4,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(4,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]); ylim([0 1])
    ylabel('Mean Accuracy'); xlabel('# of targets');
    ylim([.5 1]);
    title('OA')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    suptitle('sorted by feature accuracy L1')
    
%% sort by feature performance in L1 (best, second, ...)

for i_sub = 1:size(dat_ya,1)
    [~, idx_feat_ya] = sort(dat_ya(i_sub,:,1), 'descend');
    %idx_feat_ya = [1,2,3,4];
    data_ya_matched(1:4, i_sub,:) = squeeze(dat_ya(i_sub,idx_feat_ya,:));
    rt_ya_matched(1:4, i_sub,:) = dat_ya_rt(i_sub,idx_feat_ya,:);
end

for i_sub = 1:size(dat_oa,1)
    [~, idx_feat_oa] = sort(dat_oa(i_sub,:,1), 'descend');
    %idx_feat_oa = [1,2,3,4];
    data_oa_matched(1:4, i_sub,:) = dat_oa(i_sub,idx_feat_oa,:);
    rt_oa_matched(1:4, i_sub,:) = dat_oa_rt(i_sub,idx_feat_oa,:);
end

%% raw

% Note: these are standard errors, but for now includign all subs
figure;
subplot(1,2,1);
    hold on;
    error = squeeze(nanstd(nanmean(data_ya_matched(1,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(1,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(2,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(2,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(3,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(3,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(4,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(4,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylim([0.5 1]);
    ylabel('Mean Accuracy'); xlabel('# of targets');
    %ylim([-0.2 .05]);
    title('YA')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
subplot(1,2,2);
    hold on;
    error = squeeze(nanstd(nanmean(data_oa_matched(1,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(2,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(2,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(3,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(3,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(4,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(4,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Mean Accuracy'); xlabel('# of targets');
    ylim([0.5 1]);
    title('OA')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    suptitle('sorted by feature accuracy L1')
    
%% plot in the same plot

% Note: these are standard errors, but for now includign all subs
figure;
    hold on;
    error = squeeze(nanstd(nanmean(rt_ya_matched(1,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(rt_ya_matched(1,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_ya_matched(2,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(rt_ya_matched(2,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_ya_matched(3,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(rt_ya_matched(3,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_ya_matched(4,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(rt_ya_matched(4,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    % add YAs
    error = squeeze(nanstd(nanmean(rt_oa_matched(1,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(rt_oa_matched(1,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_oa_matched(2,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(rt_oa_matched(2,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_oa_matched(3,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(rt_oa_matched(3,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_oa_matched(4,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(rt_oa_matched(4,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Mean RT'); xlabel('# of targets');
    ylim([0.3 1.1]);
    %title('OA')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    suptitle('sorted by feature accuracy L1');

%% average across features

figure;
    hold on;
    error = squeeze(nanstd(nanmean(data_ya_matched(1:4,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(1:4,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
   
    error = squeeze(nanstd(nanmean(data_oa_matched(1:4,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:4,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Mean Accuracy'); xlabel('# of targets');
    ylim([0.5 1]);
    legend({'YA'; 'OA'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    
   %%
    
figure;
    hold on;
    error = squeeze(nanstd(nanmean(data_ya_matched(1,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(1,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(2,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(2,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(3,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(3,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(4,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(4,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    % add YAs
    error = squeeze(nanstd(nanmean(data_oa_matched(1,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(2,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(2,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(3,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(3,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(4,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(4,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Mean Accuracy'); xlabel('# of targets');
    ylim([0.5 1]);
    %title('OA')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    suptitle('sorted by feature accuracy L1');

%% only include matched L1 feature conditions (absolute)

figure;
    hold on;
    error = squeeze(nanstd(nanmean(data_ya_matched(2:4,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(2:4,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(1:3,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:3,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Mean Accuracy'); xlabel('# of targets');
    title("Matched features only")
    set(findall(gcf,'-property','FontSize'),'FontSize',18);

    figure;
    hold on;
    error = squeeze(nanstd(nanmean(rt_ya_matched(2:4,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(rt_ya_matched(2:4,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_oa_matched(1:3,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(rt_oa_matched(1:3,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Median RT'); xlabel('# of targets');
    title("Matched features only")
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    
%% baseline-correct

data_ya_matched = (data_ya_matched-repmat(data_ya_matched(:,:,1),1,1,4))./repmat(data_ya_matched(:,:,1),1,1,4);
data_oa_matched = (data_oa_matched-repmat(data_oa_matched(:,:,1),1,1,4))./repmat(data_oa_matched(:,:,1),1,1,4);
rt_ya_matched = (rt_ya_matched-repmat(rt_ya_matched(:,:,1),1,1,4))./repmat(rt_ya_matched(:,:,1),1,1,4);
rt_oa_matched = (rt_oa_matched-repmat(rt_oa_matched(:,:,1),1,1,4))./repmat(rt_oa_matched(:,:,1),1,1,4);

% Note: these are standard errors, but for now includign all subs
figure;
%subplot(1,2,1);
    hold on;
%     error = squeeze(nanstd(nanmean(data_ya_matched(1,:,:),1),[],2))./sqrt(42);
%     errorbar(squeeze(nanmean(nanmean(data_ya_matched(1,:,:),1),2)), error, 'k', ...
%         'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(2,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(2,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(3,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(3,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(4,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(4,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Mean Accuracy (% change)'); xlabel('# of targets');
    ylim([-0.2 0.2]);
    title('YA')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
%subplot(1,2,2);
    hold on;
    error = squeeze(nanstd(nanmean(data_oa_matched(1,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(2,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(2,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(3,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(3,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
%     error = squeeze(nanstd(nanmean(data_oa_matched(4,:,:),1),[],2))./sqrt(53);
%     errorbar(squeeze(nanmean(nanmean(data_oa_matched(4,:,:),1),2)), error, 'r', ...
%         'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Mean Accuracy (% change)'); xlabel('# of targets');
    ylim([-0.2 0.2]);
    title('OA')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    suptitle('sorted by feature accuracy L1')
 
%% only include matched L1 feature conditions (rel. change)

figure;
    hold on;
    error = squeeze(nanstd(nanmean(data_ya_matched(2:4,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(2:4,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(1:3,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:3,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Mean Accuracy (% change)'); xlabel('# of targets');
    ylim([-0.2 0]);
    set(findall(gcf,'-property','FontSize'),'FontSize',18);

