clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data         = fullfile(pn.root, 'data');
    pn.tools    = fullfile(pn.root, 'tools'); addpath(pn.tools);
    pn.bars     = fullfile(pn.root, 'tools', 'barwitherr'); addpath(pn.bars);
    pn.brewer   = fullfile(pn.root, 'tools', 'BrewerMap'); addpath(pn.brewer);
    pn.ploterr  = fullfile(pn.root, 'tools', 'ploterr'); addpath(pn.ploterr);
pn.plotFolder   = fullfile(pn.root, 'figures');

load(fullfile(pn.data, 'SummaryData_N102.mat'), 'SummaryData', 'IDs_all');

%SummaryData.EEG.RTs_mean % ID*Attribute*Dimension

filename = fullfile(pn.root, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

% select subjects (multimodal only)
idxMulti_summary = ismember(IDs_all, IDs_EEGMR);
[IDs_all(idxMulti_summary), IDs_EEGMR]

% define YA and OA
idx_YA = cellfun(@str2num, IDs_all)<2000;
idx_OA = cellfun(@str2num, IDs_all)>2000;

%% Propensity score matching approach

% acc_merged = squeeze(nanmean(cat(4, SummaryData.EEG.Acc_mean, SummaryData.MRI.Acc_mean),4));
% 
% for indAtt = 1:4
%     dat_ya = squeeze(nanmean(acc_merged(idxMulti_summary & idx_YA,indAtt,1:4),2));
%     dat_oa = squeeze(nanmean(acc_merged(idxMulti_summary & idx_OA,indAtt,1:4),2));
%     
%     supportrange = .001;
%     [row, column] = find(abs(dat_ya(:,1)-dat_oa(:,1)')<supportrange);
%     row = unique(row);
%     column = unique(column);
%     
%     figure; hold on;
%     plot(nanmean(dat_ya(row,:),1))
%     plot(nanmean(dat_oa(column,:),1))
%     
%     data_ya_matched(indAtt, :,:) = dat_ya; data_ya_matched(indAtt, :,:) = NaN;
%     data_ya_matched(indAtt, row,:) = dat_ya(row,:);
%     
%     data_oa_matched(indAtt, :,:) = dat_oa; data_oa_matched(indAtt, :,:) = NaN;
%     data_oa_matched(indAtt, column,:) = dat_oa(column,:);
%     
%     [h, p] = ttest2(dat_ya(row,1), dat_oa(column,1))
% end
% 
% figure;
% hold on;
% plot(squeeze(nanmean(nanmean(data_ya_matched(1:4,:,:),2),1)), 'k');
% plot(squeeze(nanmean(nanmean(data_oa_matched(1:4,:,:),2),1)), 'r');
% 
% [h, p] = ttest2(nanmedian(data_ya_matched(:,:,1),1), nanmedian(data_oa_matched(:,:,1),1))

%% RTs

% acc_merged = squeeze(nanmean(cat(4, SummaryData.EEG.RTs_mean, SummaryData.MRI.RTs_mean),4));
% 
% for indAtt = 1:4
%     dat_ya = squeeze(nanmean(acc_merged(idxMulti_summary & idx_YA,indAtt,1:4),2));
%     dat_oa = squeeze(nanmean(acc_merged(idxMulti_summary & idx_OA,indAtt,1:4),2));
%     
%     supportrange = .01;
%     [row, column] = find(abs(dat_ya(:,1)-dat_oa(:,1)')<supportrange);
%     row = unique(row);
%     column = unique(column);
%     
%     figure; hold on;
%     plot(nanmean(dat_ya(row,:),1))
%     plot(nanmean(dat_oa(column,:),1))
%     
%     data_ya_matched(indAtt, :,:) = dat_ya; data_ya_matched(indAtt, :,:) = NaN;
%     data_ya_matched(indAtt, row,:) = dat_ya(row,:);
%     
%     data_oa_matched(indAtt, :,:) = dat_oa; data_oa_matched(indAtt, :,:) = NaN;
%     data_oa_matched(indAtt, column,:) = dat_oa(column,:);
%     
%     [h, p] = ttest2(dat_ya(row,1), dat_oa(column,1))
% end
% 
% figure;
% hold on;
% plot(squeeze(nanmean(nanmean(data_ya_matched(1:4,:,:),2),1)), 'k');
% plot(squeeze(nanmean(nanmean(data_oa_matched(1:4,:,:),2),1)), 'r');
% 
% [h, p] = ttest2(nanmean(data_ya_matched(:,:,1),1), nanmean(data_oa_matched(:,:,1),1))

%% variability across features by age group

acc_merged = squeeze(cat(2, SummaryData.EEG.Acc_mean, SummaryData.MRI.Acc_mean));
%acc_merged = squeeze(cat(2, SummaryData.EEG.RTs_mean, SummaryData.MRI.RTs_mean));

dat_ya = squeeze(acc_merged(idxMulti_summary & idx_YA,:,1:4));
dat_oa = squeeze(acc_merged(idxMulti_summary & idx_OA,:,1:4));

x1 = squeeze(nanstd(dat_ya(:,:,1),[],2));
x2 = squeeze(nanstd(dat_oa(:,:,1),[],2));

y1 = squeeze(nanstd(dat_ya(:,:,4),[],2));
y2 = squeeze(nanstd(dat_oa(:,:,4),[],2));

h = figure('units','normalized','position',[.1 .1 .15 .2]);
    set(0, 'DefaultFigureRenderer', 'painters');
    hold on;
    bar(1-.2, nanmean(x1), 'FaceColor',  [1 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    bar(1+.2, nanmean(x2), 'FaceColor',  [.8 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    bar(2-.2, nanmean(y1), 'FaceColor',  [1 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    bar(2+.2, nanmean(y2), 'FaceColor',  [.8 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    % show standard error on top
    h1 = ploterr(1-.2, nanmean(x1,1), [], nanstd(x1,[],1)./sqrt(size(x1,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); set(h1(2), 'LineWidth', 4);
    h2 = ploterr(1+.2, nanmean(x2,1), [], nanstd(x2,[],1)./sqrt(size(x2,1)), 'k.', 'abshhxy', 0);
    set(h2(1), 'marker', 'none'); set(h2(2), 'LineWidth', 4);
    h1 = ploterr(2-.2, nanmean(y1,1), [], nanstd(y1,[],1)./sqrt(size(y1,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); set(h1(2), 'LineWidth', 4);
    h2 = ploterr(2+.2, nanmean(y2,1), [], nanstd(y2,[],1)./sqrt(size(y2,1)), 'k.', 'abshhxy', 0);
    set(h2(1), 'marker', 'none'); set(h2(2), 'LineWidth', 4);
    [h, pval] = ttest2(x1(:,1), x2(:,1)); mysigstar(gca, [1], .05, pval);
    [h, pval] = ttest2(y1(:,1), y2(:,1)); mysigstar(gca, [2], .05, pval);
    ylabel({'SD Accuracy';'(across features)'}); %xlabel('# of targets');
    legend({'YA'; 'OA'})
    set(gca, 'xtick', [1 2], 'xticklabel', {'L1'; 'L4'}, 'xlim', [0.5 2.5]);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)


	[h, pval] = ttest(x1(:,1), y1(:,1))
	[h, pval] = ttest(x2(:,1), y2(:,1))

%% perform separately for EEG and fMRI

acc_merged = squeeze(cat(2, SummaryData.EEG.Acc_mean, SummaryData.MRI.Acc_mean));
rt_merged = squeeze(cat(2, SummaryData.EEG.RTs_mean, SummaryData.MRI.RTs_mean));

for indAtt = 1:8
    dat_ya = squeeze(nanmean(acc_merged(idxMulti_summary & idx_YA,indAtt,1:4),2));
    dat_oa = squeeze(nanmean(acc_merged(idxMulti_summary & idx_OA,indAtt,1:4),2));
    
    dat_ya_rt = squeeze(nanmean(rt_merged(idxMulti_summary & idx_YA,indAtt,1:4),2));
    dat_oa_rt = squeeze(nanmean(rt_merged(idxMulti_summary & idx_OA,indAtt,1:4),2));
    
    supportrange = .01;
    %[row, column] = find((dat_ya(:,1)-dat_oa(:,1)')<-.01);
%     [row, column] = find((dat_oa(:,1)-dat_ya(:,1)')>0);
    
    [matchings] = matchpairs(abs(dat_ya(:,1)-dat_oa(:,1)'), supportrange, 'min');
    
    
%    [row, column] = find(abs(dat_ya(:,1)-dat_oa(:,1)')<supportrange);
    row = unique(matchings(:,1));
    column = unique(matchings(:,2));
    
    mappings{indAtt,1} = row;
    mappings{indAtt,2} = column;
    
%     figure; hold on;
%     plot(nanmean(dat_ya(row,:),1))
%     plot(nanmean(dat_oa(column,:),1))
    
    data_ya_matched(indAtt, :,:) = dat_ya; data_ya_matched(indAtt, :,:) = NaN;
    data_ya_matched(indAtt, row,:) = dat_ya(row,:);
    data_ya_unmatched(indAtt, :,:) = dat_ya; data_ya_unmatched(indAtt, :,:) = NaN;
    data_ya_unmatched(indAtt, setdiff(1:end,row),:) = dat_ya(setdiff(1:end,row),:);
    
    data_oa_matched(indAtt, :,:) = dat_oa; data_oa_matched(indAtt, :,:) = NaN;
    data_oa_matched(indAtt, column,:) = dat_oa(column,:);
    data_oa_unmatched(indAtt, :,:) = dat_oa; data_oa_unmatched(indAtt, :,:) = NaN;
    data_oa_unmatched(indAtt, setdiff(1:end,column),:) = dat_oa(setdiff(1:end,column),:);
    
    rt_ya_matched(indAtt, :,:) = dat_ya_rt; rt_ya_matched(indAtt, :,:) = NaN;
    rt_ya_matched(indAtt, row,:) = dat_ya_rt(row,:);
    rt_ya_unmatched(indAtt, :,:) = dat_ya_rt; rt_ya_unmatched(indAtt, :,:) = NaN;
    rt_ya_unmatched(indAtt, setdiff(1:end,row),:) = dat_ya_rt(setdiff(1:end,row),:);
    
    rt_oa_matched(indAtt, :,:) = dat_oa_rt; rt_oa_matched(indAtt, :,:) = NaN;
    rt_oa_matched(indAtt, column,:) = dat_oa_rt(column,:);
    rt_oa_unmatched(indAtt, :,:) = dat_oa_rt; rt_oa_unmatched(indAtt, :,:) = NaN;
    rt_oa_unmatched(indAtt, setdiff(1:end,column),:) = dat_oa_rt(setdiff(1:end,column),:);
    %[h, p] = ttest2(dat_ya(row,1), dat_oa(column,1))
end

% get subject contributions

SubCount = zeros(53,1);
for indID = 1:53
    for indAtt = 1:8
        if ismember(indID, mappings{indAtt,2})
            SubCount(indID) = SubCount(indID)+1;
        end
    end
end

figure; bar(SubCount./8); 
ylabel("percent included in matched set")
xlabel('OA participant ID')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

OAidx = find(SubCount./8 >= .75);

SubCount = zeros(42,1);
for indID = 1:42
    for indAtt = 1:8
        if ismember(indID, mappings{indAtt,1})
            SubCount(indID) = SubCount(indID)+1;
        end
    end
end

figure; bar(SubCount./8); 
ylabel("percent included in matched set")
xlabel('YA participant ID')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

YAidx = find(SubCount./8 >= .75);

save(fullfile(pn.data, 'matchedIDs.mat'), 'OAidx', 'YAidx');

figure;
hold on;
error = squeeze(nanstd(dat_ya(YAidx,:),[],1))./sqrt(numel(YAidx));
errorbar(squeeze(nanmean(dat_ya(YAidx,:),1)), error, 'k', ...
    'linewidth', 2);
error = squeeze(nanstd(dat_oa(OAidx,:),[],1))./sqrt(numel(OAidx));
errorbar(squeeze(nanmean(dat_oa(OAidx,:),1)), error, 'r', ...
    'linewidth', 2);
set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
        'xlim', [0.5 4.5]); ylim([0 1])
ylabel('Mean Accuracy'); xlabel('# of targets');
ylim([.5 .8])
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend({'YA'; 'OA'})
title("L1 acc matched")

%% plot matched and unmatched data

% Note: these are standard errors, but for now includign al subs
figure;
hold on;
error = squeeze(nanstd(nanmean(data_ya_matched(1:8,:,:),1),[],2))./sqrt(42);
errorbar(squeeze(nanmean(nanmean(data_ya_matched(1:8,:,:),1),2)), error, 'k', ...
    'linewidth', 2);
error = squeeze(nanstd(nanmean(data_oa_matched(1:8,:,:),1),[],2))./sqrt(52);
errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:8,:,:),1),2)), error, 'r', ...
    'linewidth', 2);
set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
        'xlim', [0.5 4.5]); ylim([0 1])
ylabel('Mean Accuracy'); xlabel('# of targets');
ylim([.6 .9])
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend({'YA'; 'OA'})
title("L1 acc matched")

figure;
hold on;
error = squeeze(mean(nanstd(rt_ya_matched(1:8,:,:),[],2),1))./sqrt(42);
errorbar(squeeze(mean(nanmean(rt_ya_matched(1:8,:,:),2),1)), error, 'k', ...
    'linewidth', 2);
error = squeeze(mean(nanstd(rt_oa_matched(1:8,:,:),[],2),1))./sqrt(53);
errorbar(squeeze(mean(nanmean(rt_oa_matched(1:8,:,:),2),1)), error, 'r', ...
    'linewidth', 2);
set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
        'xlim', [0.5 4.5]); ylim([0 1])
ylabel('Mean RT'); xlabel('# of targets');
ylim([.4 1.1])
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend({'YA'; 'OA'})
title("L1 acc matched")

figure;
hold on;
error = squeeze(nanstd(nanmean(data_ya_unmatched(1:8,:,:),1),[],2))./sqrt(42);
errorbar(squeeze(nanmean(nanmean(data_ya_unmatched(1:8,:,:),1),2)), error, 'k', ...
    'linewidth', 2);
error = squeeze(nanstd(nanmean(data_oa_unmatched(1:8,:,:),1),[],2))./sqrt(52);
errorbar(squeeze(nanmean(nanmean(data_oa_unmatched(1:8,:,:),1),2)), error, 'r', ...
    'linewidth', 2);
set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
        'xlim', [0.5 4.5]); ylim([0 1])
ylabel('Mean Accuracy'); xlabel('# of targets');
ylim([.55 .95])
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend({'YA'; 'OA'})
title("L1 acc unmatched")

figure;
hold on;
error = squeeze(mean(nanstd(rt_ya_unmatched(1:8,:,:),[],2),1))./sqrt(42);
errorbar(squeeze(mean(nanmean(rt_ya_unmatched(1:8,:,:),2),1)), error, 'k', ...
    'linewidth', 2);
error = squeeze(mean(nanstd(rt_oa_unmatched(1:8,:,:),[],2),1))./sqrt(53);
errorbar(squeeze(mean(nanmean(rt_oa_unmatched(1:8,:,:),2),1)), error, 'r', ...
    'linewidth', 2);
set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
        'xlim', [0.5 4.5]); ylim([0 1])
ylabel('Mean RT'); xlabel('# of targets');
ylim([.35 1])
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend({'YA'; 'OA'})
title("L1 acc unmatched")

% figure;
% hold on;
% plot(squeeze(mean(nanmean(rt_ya_matched(1:8,:,:),2),1)), 'k');
% plot(squeeze(mean(nanmean(rt_oa_matched(1:8,:,:),2),1)), 'r');

[h, p] = ttest2(nanmean(data_ya_matched(:,:,1),1), nanmean(data_oa_matched(:,:,1),1))
[h, p] = ttest2(nanmean(data_ya_matched(:,:,4),1), nanmean(data_oa_matched(:,:,4),1))
[h, p] = ttest2(nanmean(rt_ya_matched(:,:,1),1), nanmean(rt_oa_matched(:,:,1),1))

%% add figures

% Note: these are standard errors, but for now includign all subs
figure;
subplot(1,2,1);
    hold on;
    error = squeeze(nanstd(nanmean(data_ya_matched(1:8,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(1:8,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(1:8,:,:),1),[],2))./sqrt(52);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:8,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_unmatched(1:8,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_unmatched(1:8,:,:),1),2)), error, 'k--', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_unmatched(1:8,:,:),1),[],2))./sqrt(52);
    errorbar(squeeze(nanmean(nanmean(data_oa_unmatched(1:8,:,:),1),2)), error, 'r--', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]); ylim([0 1])
    ylabel('Mean Accuracy'); xlabel('# of targets');
    ylim([.5 1]);
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    legend({'YA'; 'OA'; 'YA unmatch'; 'OA unmatch'});
    %title("L1 acc matched")
subplot(1,2,2);
    hold on;
    error = squeeze(mean(nanstd(rt_ya_matched(1:8,:,:),[],2),1))./sqrt(42);
    errorbar(squeeze(mean(nanmean(rt_ya_matched(1:8,:,:),2),1)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(mean(nanstd(rt_oa_matched(1:8,:,:),[],2),1))./sqrt(53);
    errorbar(squeeze(mean(nanmean(rt_oa_matched(1:8,:,:),2),1)), error, 'r', ...
        'linewidth', 2);
    %
    error = squeeze(mean(nanstd(rt_ya_unmatched(1:8,:,:),[],2),1))./sqrt(42);
    errorbar(squeeze(mean(nanmean(rt_ya_unmatched(1:8,:,:),2),1)), error, 'k--', ...
        'linewidth', 2);
    error = squeeze(mean(nanstd(rt_oa_unmatched(1:8,:,:),[],2),1))./sqrt(53);
    errorbar(squeeze(mean(nanmean(rt_oa_unmatched(1:8,:,:),2),1)), error, 'r--', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]); ylim([0 1])
    ylabel('Mean RT'); xlabel('# of targets');
    ylim([.4 1.1]);
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    legend({'YA'; 'OA'; 'YA unmatch'; 'OA unmatch'}, ...
        'location', 'southeast');
    % title("L1 acc matched")

 %% plot matched YA-OA pairs by condition
 
 for indCond = 1:8
    figure;
    subplot(1,2,1);
        hold on;
        error = squeeze(nanstd(nanmean(data_ya_matched(indCond,:,:),1),[],2))./sqrt(42);
        errorbar(squeeze(nanmean(nanmean(data_ya_matched(indCond,:,:),1),2)), error, 'k', ...
            'linewidth', 2);
        error = squeeze(nanstd(nanmean(data_oa_matched(indCond,:,:),1),[],2))./sqrt(52);
        errorbar(squeeze(nanmean(nanmean(data_oa_matched(indCond,:,:),1),2)), error, 'r', ...
            'linewidth', 2);
        error = squeeze(nanstd(nanmean(data_ya_unmatched(indCond,:,:),1),[],2))./sqrt(42);
        errorbar(squeeze(nanmean(nanmean(data_ya_unmatched(indCond,:,:),1),2)), error, 'k--', ...
            'linewidth', 2);
        error = squeeze(nanstd(nanmean(data_oa_unmatched(indCond,:,:),1),[],2))./sqrt(52);
        errorbar(squeeze(nanmean(nanmean(data_oa_unmatched(indCond,:,:),1),2)), error, 'r--', ...
            'linewidth', 2);
        set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
                'xlim', [0.5 4.5]); ylim([0 1])
        ylabel('Mean Accuracy'); xlabel('# of targets');
        ylim([.5 1]);
        set(findall(gcf,'-property','FontSize'),'FontSize',18);
        legend({'YA'; 'OA'; 'YA unmatch'; 'OA unmatch'});
        %title("L1 acc matched")
    subplot(1,2,2);
        hold on;
        error = squeeze(mean(nanstd(rt_ya_matched(indCond,:,:),[],2),1))./sqrt(42);
        errorbar(squeeze(mean(nanmean(rt_ya_matched(indCond,:,:),2),1)), error, 'k', ...
            'linewidth', 2);
        error = squeeze(mean(nanstd(rt_oa_matched(indCond,:,:),[],2),1))./sqrt(53);
        errorbar(squeeze(mean(nanmean(rt_oa_matched(indCond,:,:),2),1)), error, 'r', ...
            'linewidth', 2);
        %
        error = squeeze(mean(nanstd(rt_ya_unmatched(indCond,:,:),[],2),1))./sqrt(42);
        errorbar(squeeze(mean(nanmean(rt_ya_unmatched(indCond,:,:),2),1)), error, 'k--', ...
            'linewidth', 2);
        error = squeeze(mean(nanstd(rt_oa_unmatched(indCond,:,:),[],2),1))./sqrt(53);
        errorbar(squeeze(mean(nanmean(rt_oa_unmatched(indCond,:,:),2),1)), error, 'r--', ...
            'linewidth', 2);
        set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
                'xlim', [0.5 4.5]); ylim([0 1])
        ylabel('Mean RT'); xlabel('# of targets');
        ylim([.4 1.1]);
        set(findall(gcf,'-property','FontSize'),'FontSize',18);
        legend({'YA'; 'OA'; 'YA unmatch'; 'OA unmatch'}, ...
            'location', 'southeast');
        % title("L1 acc matched")
        title(["cond ", num2str(indCond)])
 end