% Calculate summaries of descriptive statistics for log-RT and accuracy

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data         = fullfile(pn.root, 'data');
pn.bars         = fullfile(pn.root, 'tools', 'barwitherr'); addpath(pn.bars);
pn.brewer       = fullfile(pn.root, 'tools', 'BrewerMap'); addpath(pn.brewer);
pn.shaded       = fullfile(pn.root, 'tools', 'shadedErrorBar'); addpath(pn.shaded);
pn.plotFolder   = fullfile(pn.root, 'figures');

load(fullfile(pn.data, 'SummaryData_N102.mat'), 'SummaryData');

%% setup parameters

setup.OutlierCorrectionRT = 1;
setup.OutlierCorrectionAcc = 0; % really doesn't make any sense for accuracies

%% load merged data

load(fullfile(pn.data, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'), 'MergedDataEEG', 'MergedDataMRI', 'IDs_all');

%% compute descriptive summaries

Rawdata.EEG = MergedDataEEG;
Rawdata.MRI = MergedDataMRI;

datatypes = {'EEG'; 'MRI'};

RTs_byDim = []; Accs_byDim = [];
for indType = 1:numel(datatypes)
    RTs = Rawdata.(datatypes{indType}).RTs;
    RTs_reshaped = reshape(Rawdata.(datatypes{indType}).RTs,[8,32,102]);
    RTs_byDim(indType,:,:) = squeeze(nanmean(RTs_reshaped,2));
    %RTs_byDim = zscore(RTs_byDim, [],1);

    Acc = Rawdata.(datatypes{indType}).Accs;
    Accs_reshaped = reshape(Rawdata.(datatypes{indType}).Accs,[8,32,102]);
    Accs_byDim(indType,:,:) = squeeze(nanmean(Accs_reshaped,2));
    %Accs_byDim = zscore(Accs_byDim, [],1);
    
end % EEG/MRI

h = figure; 
subplot(2,2,1); bar(squeeze(nanmean(nanmean(RTs_byDim(:,:,1:47),1),3))); ...
    ylim([.6 .7]); ylabel('rt'); title('ya, eeg/mr avg'); 
subplot(2,2,2); bar(squeeze(nanmean(nanmean(RTs_byDim(:,:,48:end),1),3))); ...
    ylim([.75 .85]); ylabel('rt'); title('oa, eeg/mr avg'); 
subplot(2,2,3); bar(squeeze(nanmean(nanmean(Accs_byDim(:,:,1:47),1),3))); ...
    ylim([.8 1]); ylabel('acc'); title('ya, eeg/mr avg'); 
subplot(2,2,4); bar(squeeze(nanmean(nanmean(Accs_byDim(:,:,48:end),1),3))); ...
    ylim([.6 .75]); ylabel('acc'); title('oa, eeg/mr avg');
set(findall(gcf,'-property','FontSize'),'FontSize',18)
suptitle('within-block repetitions')

figureName = 'c03_within-block';
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% plot block tot

RTs_byDim = []; Accs_byDim = [];
for indType = 1:numel(datatypes)
    RTs = Rawdata.(datatypes{indType}).RTs;
    RTs_reshaped = reshape(Rawdata.(datatypes{indType}).RTs,[32,8,102]);
    RTs_byDim(indType,:,:) = squeeze(nanmean(RTs_reshaped,1));

    Acc = Rawdata.(datatypes{indType}).Accs;
    Accs_reshaped = reshape(Rawdata.(datatypes{indType}).Accs,[32,8,102]);
    Accs_byDim(indType,:,:) = squeeze(nanmean(Accs_reshaped,1));
    
end % EEG/MRI

h = figure; 
subplot(2,2,1); bar(squeeze(nanmean(nanmean(RTs_byDim(:,:,1:47),1),3))); ...
    ylim([.6 .7]); ylabel('rt'); title('ya, eeg/mr avg'); 
subplot(2,2,2); bar(squeeze(nanmean(nanmean(RTs_byDim(:,:,48:end),1),3))); ...
    ylim([.75 .85]); ylabel('rt'); title('oa, eeg/mr avg'); 
subplot(2,2,3); bar(squeeze(nanmean(nanmean(Accs_byDim(:,:,1:47),1),3))); ...
    ylim([.8 1]); ylabel('acc'); title('ya, eeg/mr avg'); 
subplot(2,2,4); bar(squeeze(nanmean(nanmean(Accs_byDim(:,:,48:end),1),3))); ...
    ylim([.6 .75]); ylabel('acc'); title('oa, eeg/mr avg');
set(findall(gcf,'-property','FontSize'),'FontSize',18)
suptitle('time on task [block]')

figureName = 'c03_tot';
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% plot RT by within-block repetitions

tmp_data = reshape(log(Rawdata.(datatypes{indType}).RTs), 8,32,102);
tmp_data = squeeze(nanmean(tmp_data,2));
tmp_data = permute(tmp_data, [2,1]);

figure; 
subplot(1,2,1); hold on;
standError = nanstd(tmp_data(idx_YA,:),1)./sqrt(size(tmp_data(idx_YA,:),1));
l1 = shadedErrorBar(1:8,nanmean(tmp_data(idx_YA,:),1),standError, 'lineprops', ...
    {'color', [1 .5 .5],'linewidth', 2}, 'patchSaturation', .1);
standError = nanstd(tmp_data(idx_OA,:),1)./sqrt(size(tmp_data(idx_OA,:),1));
l2 = shadedErrorBar(1:8,nanmean(tmp_data(idx_OA,:),1),standError, 'lineprops', ...
    {'color', [.5 .5 .5],'linewidth', 2}, 'patchSaturation', .1);

tmp_data = reshape(Rawdata.(datatypes{indType}).Accs, 8,32,102);
tmp_data = squeeze(nanmean(tmp_data,2));
tmp_data = permute(tmp_data, [2,1]);

subplot(1,2,2); hold on; cla;
standError = nanstd(tmp_data(idx_YA,:),1)./sqrt(size(tmp_data(idx_YA,:),1));
l1 = shadedErrorBar([],nanmean(tmp_data(idx_YA,:),1),standError, 'lineprops', ...
    {'color', [1 .5 .5],'linewidth', 2}, 'patchSaturation', .1);
standError = nanstd(tmp_data(idx_OA,:),1)./sqrt(size(tmp_data(idx_OA,:),1));
l2 = shadedErrorBar([],nanmean(tmp_data(idx_OA,:),1),standError, 'lineprops', ...
    {'color', [.5 .5 .5],'linewidth', 2}, 'patchSaturation', .1);

%% plot by block number (i.e., tot effect)

% the second dim now contains the 32nd trial, should be every block onset?
tmp_data = reshape(log(Rawdata.(datatypes{indType}).RTs), 32,8,102);
tmp_data = squeeze(nanmean(tmp_data,1));
tmp_data = permute(tmp_data, [2,1]);

figure; 
subplot(1,2,1); hold on;
standError = nanstd(tmp_data(idx_YA,:),1)./sqrt(size(tmp_data(idx_YA,:),1));
l1 = shadedErrorBar(1:8,nanmean(tmp_data(idx_YA,:),1),standError, 'lineprops', ...
    {'color', [1 .5 .5],'linewidth', 2}, 'patchSaturation', .1);
standError = nanstd(tmp_data(idx_OA,:),1)./sqrt(size(tmp_data(idx_OA,:),1));
l2 = shadedErrorBar(1:8,nanmean(tmp_data(idx_OA,:),1),standError, 'lineprops', ...
    {'color', [.5 .5 .5],'linewidth', 2}, 'patchSaturation', .1);

tmp_data = reshape(Rawdata.(datatypes{indType}).Accs, 32,8,102);
tmp_data = squeeze(nanmean(tmp_data,1));
tmp_data = permute(tmp_data, [2,1]);

subplot(1,2,2); hold on; cla;
standError = nanstd(tmp_data(idx_YA,:),1)./sqrt(size(tmp_data(idx_YA,:),1));
l1 = shadedErrorBar([],nanmean(tmp_data(idx_YA,:),1),standError, 'lineprops', ...
    {'color', [1 .5 .5],'linewidth', 2}, 'patchSaturation', .1);
standError = nanstd(tmp_data(idx_OA,:),1)./sqrt(size(tmp_data(idx_OA,:),1));
l2 = shadedErrorBar([],nanmean(tmp_data(idx_OA,:),1),standError, 'lineprops', ...
    {'color', [.5 .5 .5],'linewidth', 2}, 'patchSaturation', .1);

%% identify amount of specific state transitions (should be 32 blocks)

Transitions = Rawdata.(datatypes{indType}).StateOrders(1:8:end,:);

Transitions_diff = diff(Transitions,1,1);

transitionTypes = unique(Transitions_diff); 
transitionTypes = transitionTypes(~isnan(transitionTypes));

for indID = 1:size(Transitions_diff,2)
    for indTrans = 1:numel(transitionTypes)
        TransitionCounts(indTrans, indID) = numel(find(Transitions_diff(:,indID)==transitionTypes(indTrans)));
    end
end

figure; bar(nanmean(TransitionCounts,2));

%% Acc: discretize: decreasing or increasing load

indType = 1;

Transitions = Rawdata.(datatypes{indType}).StateOrders(1:8:end,:);
Transitions_diff = diff(Transitions,1,1);
curData = Rawdata.(datatypes{indType}).Accs(1:8:end,:);
curData = curData(2:end,:);

TransitionRTs = NaN(2,102);
for indID = 1:size(Transitions_diff,2)
	TransitionRTs(1, indID) = nanmean(curData(Transitions_diff(:,indID)>0));
	TransitionRTs(2, indID) = nanmean(curData(Transitions_diff(:,indID)<0));
end

[h, p] = ttest(TransitionRTs(1,idx_YA),TransitionRTs(2,idx_YA))
[h, p] = ttest(TransitionRTs(1,idx_OA),TransitionRTs(2,idx_OA))

idx_YA = cellfun(@str2num, IDs_all)<2000;
idx_OA = cellfun(@str2num, IDs_all)>2000;
 
colorm = [43/255 140/255 190/255; 230/255 75/255 50/255; 230/255 75/255 50/255];

h = figure('units','normalized','position',[.1 .1 .15 .15]);
subplot(1,2,1);
    hold on;
    dat = squeeze(permute(TransitionRTs(:,idx_YA),[2,1]));
    bar(1:size(dat,2), nanmean(dat), 'FaceColor',  colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard error on top
    h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    set(gca, 'xtick', [1 2], 'xticklabel', {'Incr.', 'Decr.'}, ...
        'xlim', [0.5 2.5]); ylim([.7 .9])
    xlabel('Load change'); ylabel("Acc")
    title('YAs')
    set(findall(gcf,'-property','FontSize'),'FontSize',22)

 subplot(1,2,2);
    hold on;
    dat = squeeze(permute(TransitionRTs(:,idx_OA),[2,1]));
    bar(1:size(dat,2), nanmean(dat), 'FaceColor',  colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard error on top
    h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    set(gca, 'xtick', [1 2], 'xticklabel', {'Incr.', 'Decr.'}, ...
        'xlim', [0.5 2.5]); ylim([.7 .9])
    xlabel('Load change'); ylabel("Acc")
    title('OAs')
    set(findall(gcf,'-property','FontSize'),'FontSize',22)
    

%% RT: discretize: decreasing or increasing load

indType = 1;

Transitions = Rawdata.(datatypes{indType}).StateOrders(1:8:end,:);
Transitions_diff = diff(Transitions,1,1);
curData = Rawdata.(datatypes{indType}).RTs(1:8:end,:);
curData = curData(2:end,:);

TransitionRTs = NaN(2,102);
for indID = 1:size(Transitions_diff,2)
	TransitionRTs(1, indID) = nanmean(curData(Transitions_diff(:,indID)>0));
	TransitionRTs(2, indID) = nanmean(curData(Transitions_diff(:,indID)<0));
end

[h, p] = ttest(TransitionRTs(1,idx_YA),TransitionRTs(2,idx_YA))
[h, p] = ttest(TransitionRTs(1,idx_OA),TransitionRTs(2,idx_OA))

idx_YA = cellfun(@str2num, IDs_all)<2000;
idx_OA = cellfun(@str2num, IDs_all)>2000;
 
colorm = [43/255 140/255 190/255; 230/255 75/255 50/255; 230/255 75/255 50/255];

h = figure('units','normalized','position',[.1 .1 .15 .15]);
subplot(1,2,1);
    hold on;
    dat = squeeze(permute(TransitionRTs(:,idx_YA),[2,1]));
    bar(1:size(dat,2), nanmean(dat), 'FaceColor',  colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard error on top
    h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    set(gca, 'xtick', [1 2], 'xticklabel', {'Incr.', 'Decr.'}, ...
        'xlim', [0.5 2.5]); ylim([.55 .65])
    xlabel('Load change'); ylabel("RT")
    title('YAs')
    set(findall(gcf,'-property','FontSize'),'FontSize',22)

 subplot(1,2,2);
    hold on;
    dat = squeeze(permute(TransitionRTs(:,idx_OA),[2,1]));
    bar(1:size(dat,2), nanmean(dat), 'FaceColor',  colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard error on top
    h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    set(gca, 'xtick', [1 2], 'xticklabel', {'Incr.', 'Decr.'}, ...
        'xlim', [0.5 2.5]); ylim([.55 .65])
    xlabel('Load change'); ylabel("RT")
    title('OAs')
    set(findall(gcf,'-property','FontSize'),'FontSize',22)
    

%% plot RT by repetitions: by prior transition

tmp_rt = reshape(Rawdata.(datatypes{indType}).RTs, 32,8,102);
tmp_rt = tmp_rt(2:end,:,:);

tmp_acc = reshape(Rawdata.(datatypes{indType}).RTs, 32,8,102);
tmp_acc = tmp_acc(2:end,:,:);

Transitions = reshape(Rawdata.(datatypes{indType}).StateOrders,8,32,102);
Transitions_diff = diff(Transitions,[],1);

TransitionRTs = NaN(2,8,102); TransitionAccs = NaN(2,8,102);
for indID = 1:size(Transitions_diff,3)
	TransitionRTs(1,:,indID) = nanmean(tmp_rt(Transitions_diff(:,:,indID)>0));
	TransitionRTs(2,:,indID) = nanmean(tmp_rt(Transitions_diff(:,:,indID)<0));
    TransitionAccs(1,:,indID) = nanmean(tmp_acc(Transitions_diff(:,:,indID)>0));
	TransitionAccs(2,:,indID) = nanmean(tmp_acc(Transitions_diff(:,:,indID)<0));
end

tmp_data = squeeze(TransitionRTs(1,:,:));
tmp_data = permute(tmp_data, [2,1]);

figure; 
subplot(1,2,1); hold on;
standError = nanstd(tmp_data(idx_YA,:),1)./sqrt(size(tmp_data(idx_YA,:),1));
l1 = shadedErrorBar(1:8,nanmean(tmp_data(idx_YA,:),1),standError, 'lineprops', ...
    {'color', [1 .5 .5],'linewidth', 2}, 'patchSaturation', .1);
standError = nanstd(tmp_data(idx_OA,:),1)./sqrt(size(tmp_data(idx_OA,:),1));
l2 = shadedErrorBar(1:8,nanmean(tmp_data(idx_OA,:),1),standError, 'lineprops', ...
    {'color', [.5 .5 .5],'linewidth', 2}, 'patchSaturation', .1);
tmp_data = squeeze(TransitionRTs(2,:,:));
tmp_data = permute(tmp_data, [2,1]);
standError = nanstd(tmp_data(idx_YA,:),1)./sqrt(size(tmp_data(idx_YA,:),1));
l1 = shadedErrorBar(1:8,nanmean(tmp_data(idx_YA,:),1),standError, 'lineprops', ...
    {'color', [.8 .5 .5],'linewidth', 2}, 'patchSaturation', .1);
standError = nanstd(tmp_data(idx_OA,:),1)./sqrt(size(tmp_data(idx_OA,:),1));
l2 = shadedErrorBar(1:8,nanmean(tmp_data(idx_OA,:),1),standError, 'lineprops', ...
    {'color', [.3 .5 .5],'linewidth', 2}, 'patchSaturation', .1);

tmp_data = reshape(Rawdata.(datatypes{indType}).Accs, 32,8,102);
tmp_data = squeeze(nanmean(tmp_data,1));
tmp_data = permute(tmp_data, [2,1]);

subplot(1,2,2); hold on;
standError = nanstd(tmp_data(idx_YA,:),1)./sqrt(size(tmp_data(idx_YA,:),1));
l1 = shadedErrorBar(1:8,nanmean(tmp_data(idx_YA,:),1),standError, 'lineprops', ...
    {'color', [1 .5 .5],'linewidth', 2}, 'patchSaturation', .1);
standError = nanstd(tmp_data(idx_OA,:),1)./sqrt(size(tmp_data(idx_OA,:),1));
l2 = shadedErrorBar(1:8,nanmean(tmp_data(idx_OA,:),1),standError, 'lineprops', ...
    {'color', [.5 .5 .5],'linewidth', 2}, 'patchSaturation', .1);

