clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data         = fullfile(pn.root, 'data');
pn.bars         = fullfile(pn.root, 'tools', 'barwitherr'); addpath(pn.bars);
pn.brewer       = fullfile(pn.root, 'tools', 'BrewerMap'); addpath(pn.brewer);
pn.plotFolder   = fullfile(pn.root, 'figures');

load(fullfile(pn.data, 'SummaryData_N102.mat'), 'SummaryData', 'IDs_all');

%% split results by young and old

IDs_group = str2num(cellfun(@(x)x(1), IDs_all));
idx_YA = IDs_group==1;
idx_OA = IDs_group==2;
ageName(idx_YA==1) = {'YA'};
ageName(idx_OA==1) = {'OA'};

%% export EEG output

StateSwitchDynEEG{1,1} = 'subject';
StateSwitchDynEEG{1,2} = 'age';
StateSwitchDynEEG{1,4} = 'att';
StateSwitchDynEEG{1,3} = 'dim';
StateSwitchDynEEG{1,5} = 'meanlogRT';
StateSwitchDynEEG{1,6} = 'meanAcc';

count = 2;
for indID = 1:size(SummaryData.EEG.RTs_mean,1)
    for indAtt = 1:size(SummaryData.EEG.RTs_mean,2)
        for indDim = 1:size(SummaryData.EEG.RTs_mean,3)
            StateSwitchDynEEG{count,1} = indID;
            StateSwitchDynEEG{count,2} = ageName{indID};
            StateSwitchDynEEG{count,3} = indAtt;
            StateSwitchDynEEG{count,4} = indDim;
            StateSwitchDynEEG{count,5} = SummaryData.EEG.logRTs_mean(indID, indAtt, indDim);
            StateSwitchDynEEG{count,6} = SummaryData.EEG.Acc_mean(indID, indAtt, indDim);
            count = count + 1;
        end
    end
end

StateSwitchDynEEG(1,:) = [];

save(fullfile(pn.data, 'F_dataForR_EEG.mat'), 'StateSwitchDynEEG');

%% export MRI output

StateSwitchDynMRI{1,1} = 'subject';
StateSwitchDynMRI{1,2} = 'age';
StateSwitchDynMRI{1,4} = 'att';
StateSwitchDynMRI{1,3} = 'dim';
StateSwitchDynMRI{1,5} = 'meanlogRT';
StateSwitchDynMRI{1,6} = 'meanAcc';

count = 2;
for indID = 1:size(SummaryData.MRI.RTs_mean,1)
    for indAtt = 1:size(SummaryData.MRI.RTs_mean,2)
        for indDim = 1:size(SummaryData.MRI.RTs_mean,3)
            StateSwitchDynMRI{count,1} = indID;
            StateSwitchDynMRI{count,2} = ageName{indID};
            StateSwitchDynMRI{count,3} = indAtt;
            StateSwitchDynMRI{count,4} = indDim;
            StateSwitchDynMRI{count,5} = SummaryData.MRI.logRTs_mean(indID, indAtt, indDim);
            StateSwitchDynMRI{count,6} = SummaryData.MRI.Acc_mean(indID, indAtt, indDim);
            count = count + 1;
        end
    end
end

StateSwitchDynMRI(1,:) = [];

save(fullfile(pn.data, 'F_dataForR_MRI.mat'), 'StateSwitchDynMRI');
