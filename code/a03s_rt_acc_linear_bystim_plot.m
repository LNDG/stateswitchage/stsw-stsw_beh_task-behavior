% - plot linear modulation by attribute (MR, EEG) vs. zero

clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data         = fullfile(pn.root, 'data');
pn.bars         = fullfile(pn.root, 'tools', 'barwitherr'); addpath(pn.bars);
pn.brewer       = fullfile(pn.root, 'tools', 'BrewerMap'); addpath(pn.brewer);
pn.plotFolder   = fullfile(pn.root, 'figures');

load(fullfile(pn.data, 'SummaryData_N102.mat'), 'SummaryData', 'IDs_all');

%SummaryData.EEG.RTs_mean % ID*Attribute*Dimension

%% split results by young and old

IDs_group = str2num(cellfun(@(x)x(1), IDs_all));
idx_YA = IDs_group==1;
idx_OA = IDs_group==2;

%% plot more traditional plot

attributes = {'Color','Direction','Size', 'Luminance'};

colorm = [230/265 25/265 25/265; 0/265 50/265 100/265]; % use external function to create colormap

for indFeature = 1:4
    data = squeeze(SummaryData.EEG.Acc_mean(:,indFeature,1:4));
    X = [1 1; 1 2; 1 3; 1 4]; b=X\data';
    LinearLoadEffects_Acc_EEG(:,indFeature) = b(2,:);
    data = squeeze(SummaryData.EEG.logRTs_mean(:,indFeature,1:4));
    X = [1 1; 1 2; 1 3; 1 4]; b=X\data';
    LinearLoadEffects_RT_EEG(:,indFeature) = b(2,:);
    
    data = squeeze(SummaryData.MRI.Acc_mean(:,indFeature,1:4));
    X = [1 1; 1 2; 1 3; 1 4]; b=X\data';
    LinearLoadEffects_Acc_MRI(:,indFeature) = b(2,:);
    data = squeeze(SummaryData.MRI.logRTs_mean(:,indFeature,1:4));
    X = [1 1; 1 2; 1 3; 1 4]; b=X\data';
    LinearLoadEffects_RT_MRI(:,indFeature) = b(2,:);
end


h = figure('units','normalized','position',[.1 .1 .4 .2]);

subplot(1,2,1); cla;
    
    dat = LinearLoadEffects_RT_EEG(idx_YA,:); % select only load 1 for each target, only accuracy here
    dat4 = LinearLoadEffects_RT_MRI(idx_YA,:); % select only load 1 for each target, only accuracy here

    hold on;
    % if we want each bar to have a different color, loop
    for b = 1:size(dat, 2)
        bar(b-.2, nanmean(dat(:,b)), 'FaceColor',  [1 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
        bar(b+.2, nanmean(dat4(:,b)), 'FaceColor',  [.8 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    end
    %hold on; line([0 5], [.5 .5], 'Color', 'k', 'LineStyle', '--', 'LineWidth', 2)
    
    % plot jittered individual values on top
    scatter(repmat(1-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,1), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    scatter(repmat(2-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,2), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    scatter(repmat(3-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,3), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    scatter(repmat(4-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,4), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    
    scatter(repmat(1+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,1), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    scatter(repmat(2+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,2), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    scatter(repmat(3+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,3), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    scatter(repmat(4+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,4), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    
    % show standard deviation on top
    h1 = ploterr([1:size(dat,2)]-.2, nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    h2 = ploterr([1:size(dat4,2)]+.2, nanmean(dat4,1), [], nanstd(dat4,[],1)./sqrt(size(dat4,1)), 'k.', 'abshhxy', 0);
    set(h2(1), 'marker', 'none'); % remove marker
    set(h2(2), 'LineWidth', 4);

    % label what we're seeing
    % if labels are too long to fit, use the xticklabelrotation with about -30
    % to rotate them so they're readable
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', attributes, ...
        'xlim', [0.5 4.5]); %ylim([0 1])
    ylabel('linear beta'); %xlabel('# of targets');

    % if these data are paired, show the differences
    % plot(dat', '.k-', 'linewidth', 0.2, 'markersize', 2);

    for indCond = 1:4
        % significance star for the difference
        [~, pval] = ttest(dat(:, indCond), repmat(0,size(dat(:, indCond)))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        mysigstar(gca, [indCond]-.2, .02, pval);
        
        % significance star for the difference
        [~, pval] = ttest(dat4(:, indCond), repmat(0,size(dat4(:, indCond)))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        mysigstar(gca, [indCond]+.2, .02, pval);
    end
    title('mean RT');

    subplot(1,2,2); cla;
    
    dat = LinearLoadEffects_Acc_EEG(idx_YA,:); % select only load 1 for each target, only accuracy here
    dat4 = LinearLoadEffects_Acc_MRI(idx_YA,:); % select only load 1 for each target, only accuracy here

    hold on;
    % if we want each bar to have a different color, loop
    for b = 1:size(dat, 2)
        bar(b-.2, nanmean(dat(:,b)), 'FaceColor',  [1 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
        bar(b+.2, nanmean(dat4(:,b)), 'FaceColor',  [.8 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    end
    %hold on; line([0 5], [.5 .5], 'Color', 'k', 'LineStyle', '--', 'LineWidth', 2)
    
    % plot jittered individual values on top
    scatter(repmat(1-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,1), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    scatter(repmat(2-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,2), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    scatter(repmat(3-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,3), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    scatter(repmat(4-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,4), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    
    scatter(repmat(1+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,1), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    scatter(repmat(2+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,2), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    scatter(repmat(3+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,3), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    scatter(repmat(4+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,4), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    
    % show standard deviation on top
    h1 = ploterr([1:size(dat,2)]-.2, nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    h2 = ploterr([1:size(dat4,2)]+.2, nanmean(dat4,1), [], nanstd(dat4,[],1)./sqrt(size(dat4,1)), 'k.', 'abshhxy', 0);
    set(h2(1), 'marker', 'none'); % remove marker
    set(h2(2), 'LineWidth', 4);

    % label what we're seeing
    % if labels are too long to fit, use the xticklabelrotation with about -30
    % to rotate them so they're readable
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', attributes, ...
        'xlim', [0.5 4.5]); %ylim([0 1])
    ylabel('linear beta'); %xlabel('# of targets');

    % if these data are paired, show the differences
    % plot(dat', '.k-', 'linewidth', 0.2, 'markersize', 2);

    for indCond = 1:4
        % significance star for the difference
        [~, pval] = ttest(dat(:, indCond), repmat(0,size(dat(:, indCond)))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        mysigstar(gca, [indCond]-.2, .15, pval);
        
        % significance star for the difference
        [~, pval] = ttest(dat4(:, indCond), repmat(0,size(dat4(:, indCond)))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        mysigstar(gca, [indCond]+.2, .15, pval);
    end
    title('mean Accuracy');
    
    set(findall(gcf,'-property','FontSize'),'FontSize',20)

    figureName = 'E2_EEG_byAtt_supplement';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');
    