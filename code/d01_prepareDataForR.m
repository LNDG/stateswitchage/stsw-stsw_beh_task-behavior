% bring data into HDDM format:
% Columns: condition (seperate subjects here), correct/incorrect, RTs (in s) 

clear all; clc;

pn.dataIn = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/';
load([pn.dataIn, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'], 'MergedDataEEG', 'MergedDataMRI', 'IDs_all');

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/B_data/';

modalities = {'EEG'; 'MRI'};

for indModality = 1:numel(modalities)

    %% get vectors of RTs and Acc

    RTs_vector = reshape(eval(['MergedData', modalities{indModality}, '.RTs']),1,[]);
    Acc_vector = reshape(eval(['MergedData', modalities{indModality}, '.Accs']),1,[]);

    %% create vector of IDs

    Sub_vector = reshape(repmat(1:102,256,1),1,[]);

    %% create vector of age

    ageVec = [];
    IDs = cell2mat(IDs_all);
    IDs = str2mat(IDs(:,1));
    for indEntry = 1:numel(IDs)
        if strcmp(IDs(indEntry), '1')
            %ageVec{1,indEntry} = 'YA';
            ageVec(1,indEntry) = 1;
        elseif strcmp(IDs(indEntry), '2')
            %ageVec{1,indEntry} = 'OA';
            ageVec(1,indEntry) = 2;
        end
    end; clear IDs;

    Age_vector = reshape(repmat(ageVec,256,1),1,[]);

    %% create vector of dimensionality

    Dim_vector = reshape(eval(['MergedData', modalities{indModality}, '.StateOrders']),1,[]);

    %% create vector of attribute

    Att_vector = reshape(eval(['MergedData', modalities{indModality}, '.Atts']),1,[]);
    
    %% create vector of time on task (trial #)
    
    ToT = repmat([1:256]',102,1)';
    
    %% create vector of within-block repetition
    
    Repetition = repmat([1:8]',102*32,1)';
    
    %% create vector of switch (first difference in dimensionality)
    
    Switch = [zeros(1,102); diff(eval(['MergedData', modalities{indModality}, '.StateOrders']))];
    Switch = reshape(Switch,1,[]);
        
    %% model within-block switch vs. stay costs (i.e., change in probed attribute)
    
    AttSwitch = [zeros(1,102); diff(eval(['MergedData', modalities{indModality}, '.Atts']))];
    AttSwitch = reshape(AttSwitch,1,[]);
    
    % binarize: actual feature switch not relevant here
   
    AttSwitch(AttSwitch~=0) = 1;
    
    %% encode modality
    
    Modality = repmat(indModality, size(Repetition,1), size(Repetition,2));
    
    %% combine in data matrix

    tmp_data{indModality} = [Sub_vector', Acc_vector', RTs_vector', Dim_vector', Att_vector', ToT', Repetition', Switch', AttSwitch', Age_vector', Modality'];
    
end

    data = [];
    data = [tmp_data{1}; tmp_data{2}];

    %% set any RTs below 250 ms to NaN

    data(find(data(:,3)<=.25),:) = NaN;

    %figure; plot(sort(RTs_vector,'ascend')); ylim([0 .25])
    
    %% delete any rows with nans

    data(any(isnan(data), 2), :) = [];

    %% recode RTs as log(RT)
    
    data(:,3) = log(data(:,3));
    
    %% add complete ID

    IDs_mat =  cellfun(@str2num,IDs_all);
    data(:,12) = IDs_mat(data(:,1)); clear IDs_mat;

%% add headers

    data = num2cell(data);
    
    data(cell2mat(cellfun(@(x) x(:)==1, data(:,11), 'uniform', 0)),11) = {'EEG'};
    data(cell2mat(cellfun(@(x) x(:)==2, data(:,11), 'uniform', 0)),11) = {'MRI'};
    
    data = [{'subject'},{'acc'},{'rt'},{'dim'},{'att'},{'tot'},{'rep'},{'switch'}, {'attswitch'},{'age'},{'modality'},{'ID'};data];

 %% save for R

    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/cell2csv/')
    save([pn.dataOut, 'SingleTrialData_YAOA_forR.mat'],'data')
    
    cell2csv([pn.dataOut, 'SingleTrialData_YAOA_forR.csv'],data)
