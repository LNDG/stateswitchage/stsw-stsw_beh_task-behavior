clear all; clc;

pn.dataEEG_In   = '/Volumes/LNDG/Projects/StateSwitch/dynamic/raw/C_study/eeg/*';
pn.dataMRI_In   = '/Volumes/LNDG/Projects/StateSwitch/dynamic/raw/C_study/mri/behavior_eye/*';

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.dataOut = fullfile(pn.root, 'data');

%% get available behavioral subjects

% ID_MRI = dir(pn.dataMRI_In);
% ID_MRI = {ID_MRI(:).name}';
% ID_MRI = cellfun(@(x){x(1:4)}, ID_MRI);
ID_MRI = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';'1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';'1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

% ID_EEG = dir(pn.dataEEG_In);
% ID_EEG = {ID_EEG(:).name}';
% ID_EEG = cellfun(@(x){x(1:4)}, ID_EEG);

ID_EEG = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';'1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

IDs_Complete    = intersect(ID_MRI,ID_EEG);
IDs_MRIonly     = setdiff(ID_MRI,ID_EEG);
IDs_EEGonly     = setdiff(ID_EEG,ID_MRI);
IDs_all         = union(ID_MRI,ID_EEG);

%% merge information from the subjects (EEG session)

% NOTE: for subject 1223, runs 1+2 were done twice instead of runs 3+4

MergedData.StateOrders = NaN(256, numel(IDs_all));
MergedData.Atts = NaN(256, numel(IDs_all));
MergedData.RTs = NaN(256, numel(IDs_all));
MergedData.Accs = NaN(256, numel(IDs_all));

for indID = 1:numel(IDs_all)
    files = dir([pn.dataEEG_In, '/', IDs_all{indID}, '_StateSwitch_dynamic*']);
    tmp_ResultMat = NaN(256,4);
    if ~isempty(files)
        for indFile = 1:numel(files) % put together runs if there are multiple files (e.g. PTB crash between runs)
            tmp_ReadIn{indFile} = load([files(indFile).folder, '/', files(indFile).name], 'ResultMat', 'expInfo');
            idxOnset = find(~isnan(tmp_ReadIn{indFile}.ResultMat(:,1)), 1, 'first');
            idxEnd = find(~isnan(tmp_ReadIn{indFile}.ResultMat(:,1)), 1, 'last');
            % NOTE: if subject is 1223: assume runs 3+4 as 1+2
            if strcmp(IDs_all{indID}, '1223') && indFile == 2
                tmp_ResultMat(129:256, :) = tmp_ReadIn{indFile}.ResultMat(idxOnset:idxEnd, :);
            else
                tmp_ResultMat(idxOnset:idxEnd, :) = tmp_ReadIn{indFile}.ResultMat(idxOnset:idxEnd, :);
            end
            tmp_expInfo = tmp_ReadIn{indFile}.expInfo;
        end
        MergedData.expInfo{1,indID} = tmp_expInfo;
        clear tmp_ReadIn;
    end
    % extract parameters
    MergedData.StateOrders(:,indID) = squeeze(tmp_ResultMat(:,1)); % indBlock*indTrial*indMeasure:
    MergedData.Atts(:,indID) = squeeze(tmp_ResultMat(:,2));
    MergedData.RTs(:,indID) = squeeze(tmp_ResultMat(:,3));
    MergedData.Accs(:,indID) = squeeze(tmp_ResultMat(:,4));
    if strcmp(IDs_all{indID}, '2227') % invert accuracies for subject 2227
        MergedData.Accs(:,indID) = 1-squeeze(tmp_ResultMat(:,4));
    else
        MergedData.Accs(:,indID) = squeeze(tmp_ResultMat(:,4));
    end
end

MergedDataEEG = MergedData; clear MergedData;

%% merge information from the subjects (MR session)

% Note: for 2121, run 4 is in file 2; for 2132, runs 3+4 are in file 2.

MergedData.StateOrders = NaN(256, numel(IDs_all));
MergedData.Atts = NaN(256, numel(IDs_all));
MergedData.RTs = NaN(256, numel(IDs_all));
MergedData.Accs = NaN(256, numel(IDs_all));

for indID = 1:numel(IDs_all)
    files = dir([pn.dataMRI_In, '/', IDs_all{indID}, '_StateSwitchMR_dynamic*']);
    tmp_ResultMat = NaN(256,4);
    if ~isempty(files)
        for indFile = 1:numel(files) % put together runs if there are multiple files (e.g. PTB crash between runs)
            tmp_ReadIn{indFile} = load([files(indFile).folder, '/', files(indFile).name], 'ResultMat', 'expInfo');    
            if strcmp(IDs_all{indID}, '2121') & indFile == 1
                idxOnset = 1; idxEnd = 64*3;
            elseif strcmp(IDs_all{indID}, '2121') & indFile == 2
                idxOnset = 64*3+1; idxEnd = 64*4;    
            elseif strcmp(IDs_all{indID}, '2132') & indFile == 1
                idxOnset = 1; idxEnd = 64*2;    
            elseif strcmp(IDs_all{indID}, '2132') & indFile == 2
                idxOnset = 64*2+1; idxEnd = 64*4;    
            else
                idxOnset = find(~isnan(tmp_ReadIn{indFile}.ResultMat(:,1)), 1, 'first');
                idxEnd = find(~isnan(tmp_ReadIn{indFile}.ResultMat(:,1)), 1, 'last');
            end
            tmp_ResultMat(idxOnset:idxEnd, :) = tmp_ReadIn{indFile}.ResultMat(idxOnset:idxEnd, :);
            tmp_expInfo = tmp_ReadIn{indFile}.expInfo;
        end
        % NOTE: for subject 1203 accuracies were inversely coded
        if strcmp(IDs_all{indID}, '1203')
            tmp_ResultMat(1:64,4) = 1-tmp_ResultMat(1:64,4);
        end
        MergedData.expInfo{1,indID} = tmp_expInfo;
        clear tmp_ReadIn;
    end
    % extract parameters
    MergedData.StateOrders(:,indID) = squeeze(tmp_ResultMat(:,1)); % indBlock*indTrial*indMeasure:
    MergedData.Atts(:,indID) = squeeze(tmp_ResultMat(:,2));
    MergedData.RTs(:,indID) = squeeze(tmp_ResultMat(:,3));
    MergedData.Accs(:,indID) = squeeze(tmp_ResultMat(:,4));
end

MergedDataMRI = MergedData; clear MergedData;

%% add laterality of response 

for indSubject = 1:102
indCount = 1;
   for indRun = 1:4
       for indBlock = 1:8
           for indTrial = 1:8
               if ~isempty(MergedDataEEG.expInfo{indSubject})
                   MergedDataEEG.TargetOption(indCount,indSubject) = MergedDataEEG.expInfo{indSubject}.targetOptionRun{indRun}(indBlock,indTrial);
               else
                   MergedDataEEG.TargetOption(indCount,indSubject) = NaN;
               end
               if ~isempty(MergedDataMRI.expInfo{indSubject})
                   MergedDataMRI.TargetOption(indCount,indSubject) = MergedDataMRI.expInfo{indSubject}.targetOptionRun{indRun}(indBlock,indTrial);
               else
                   MergedDataMRI.TargetOption(indCount,indSubject) = NaN;
               end
                indCount = indCount+1;
           end
       end
   end
end

%% save merged data

save(fullfile(pn.dataOut, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'),...
    'MergedDataEEG', 'MergedDataMRI', 'IDs_all');

%% sanity-check the data

h = figure;
subplot(2,4,1); imagesc(MergedDataEEG.StateOrders); title('StateOrders EEG'); xlabel('Subjects'); ylabel('Trials')
subplot(2,4,2); imagesc(MergedDataEEG.Atts); title('Atts EEG'); xlabel('Subjects'); ylabel('Trials')
subplot(2,4,3); imagesc(MergedDataEEG.RTs); title('RTs EEG'); xlabel('Subjects'); ylabel('Trials')
subplot(2,4,4); imagesc(MergedDataEEG.Accs); title('Accs EEG'); xlabel('Subjects'); ylabel('Trials')

subplot(2,4,5); imagesc(MergedDataMRI.StateOrders); title('StateOrders MRI'); xlabel('Subjects'); ylabel('Trials')
subplot(2,4,6); imagesc(MergedDataMRI.Atts); title('Atts MRI'); xlabel('Subjects'); ylabel('Trials')
subplot(2,4,7); imagesc(MergedDataMRI.RTs); title('RTs MRI'); xlabel('Subjects'); ylabel('Trials')
subplot(2,4,8); imagesc(MergedDataMRI.Accs); title('Accs MRI'); xlabel('Subjects'); ylabel('Trials')

%% 2241 appears to be an outlier in EEG RTs

figure; 
subplot(2,2,1); plot(MergedDataEEG.RTs(:,87));
subplot(2,2,2); plot(MergedDataMRI.RTs(:,87));
subplot(2,2,3); plot(MergedDataEEG.Accs(:,87));
subplot(2,2,4); plot(MergedDataMRI.Accs(:,87));

[nanmean(MergedDataEEG.Accs(:,87)), nanmean(MergedDataMRI.Accs(:,87))]

% We could try to check whether there were multiple responses in the raw files.