% Calculate summaries of descriptive statistics for log-RT and accuracy

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data         = fullfile(pn.root, 'data');
pn.bars         = fullfile(pn.root, 'tools', 'barwitherr'); addpath(pn.bars);
pn.brewer       = fullfile(pn.root, 'tools', 'BrewerMap'); addpath(pn.brewer);
pn.plotFolder   = fullfile(pn.root, 'figures');

%% setup parameters

setup.OutlierCorrectionRT = 1;
setup.OutlierCorrectionAcc = 0; % really doesn't make any sense for accuracies

%% load merged data

load(fullfile(pn.data, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'), ...
    'MergedDataEEG', 'MergedDataMRI', 'IDs_all');

%% compute descriptive summaries

Rawdata.EEG = MergedDataEEG;
Rawdata.MRI = MergedDataMRI;

datatypes = {'EEG'; 'MRI'};

for indType = 1:numel(datatypes)
    RTsRemoved = zeros(numel(IDs_all),4,4); ACCsRemoved = zeros(numel(IDs_all),4,4);
    for indID = 1:numel(IDs_all)
        for indAtt = 1:4
            for indDim = 1:4
                targetTrials = find(Rawdata.(datatypes{indType}).StateOrders(:,indID)==indDim & ...
                    Rawdata.(datatypes{indType}).Atts(:,indID)==indAtt);
                % outlier correction (within subject and condition)
                if setup.OutlierCorrectionRT == 1
                    % RTs
                    check = 0;
                    while check == 0
                        A = Rawdata.(datatypes{indType}).RTs(targetTrials,indID);
                        idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                        if ~isempty(idxOutlier)
                            Rawdata.(datatypes{indType}).RTs(targetTrials(idxOutlier),indID) = NaN;
                            RTsRemoved(indID,indAtt,indDim) = RTsRemoved(indID,indAtt,indDim)+numel(idxOutlier);
                        else check = 1;
                        end
                    end
                end
                if setup.OutlierCorrectionAcc == 1
                    % accuracies
                    check = 0;
                    while check == 0
                        A = Rawdata.(datatypes{indType}).Accs(targetTrials,indID);
                        idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                        if ~isempty(idxOutlier)
                            Rawdata.(datatypes{indType}).Accs(targetTrials(idxOutlier),indID) = NaN;
                            ACCsRemoved(indID,indAtt,indDim) = ACCsRemoved(indID,indAtt,indDim)+numel(idxOutlier);
                        else check = 1;
                        end
                    end
                end
                % calculate average metrics
                SummaryData.(datatypes{indType}).logRTs_mean(indID,indAtt,indDim) = nanmean(log(Rawdata.(datatypes{indType}).RTs(targetTrials,indID)));
                SummaryData.(datatypes{indType}).RTs_mean(indID,indAtt,indDim) = nanmean(Rawdata.(datatypes{indType}).RTs(targetTrials,indID));
                SummaryData.(datatypes{indType}).Acc_mean(indID,indAtt,indDim) = nanmean(Rawdata.(datatypes{indType}).Accs(targetTrials,indID));
                SummaryData.(datatypes{indType}).logRTs_md(indID,indAtt,indDim) = nanmedian(log(Rawdata.(datatypes{indType}).RTs(targetTrials,indID)));
                SummaryData.(datatypes{indType}).RTs_md(indID,indAtt,indDim) = nanmedian(Rawdata.(datatypes{indType}).RTs(targetTrials,indID));
                SummaryData.(datatypes{indType}).Acc_md(indID,indAtt,indDim) = nanmedian(Rawdata.(datatypes{indType}).Accs(targetTrials,indID));
                SummaryData.(datatypes{indType}).logRTs_std(indID,indAtt,indDim) = nanstd(log(Rawdata.(datatypes{indType}).RTs(targetTrials,indID)));
                SummaryData.(datatypes{indType}).RTs_std(indID,indAtt,indDim) = nanstd(Rawdata.(datatypes{indType}).RTs(targetTrials,indID));
                SummaryData.(datatypes{indType}).Acc_std(indID,indAtt,indDim) = nanstd(Rawdata.(datatypes{indType}).Accs(targetTrials,indID));
            end % dimensionality
        end % attribute
    end % subject
end % EEG/MRI

save(fullfile(pn.data, 'SummaryData_N102.mat'), 'SummaryData', 'IDs_all');
