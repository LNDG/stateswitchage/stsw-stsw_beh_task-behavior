clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data         = fullfile(pn.root, 'data');
    pn.tools    = fullfile(pn.root, 'tools'); addpath(pn.tools);
    pn.bars     = fullfile(pn.root, 'tools', 'barwitherr'); addpath(pn.bars);
    pn.brewer   = fullfile(pn.root, 'tools', 'BrewerMap'); addpath(pn.brewer);
    pn.ploterr  = fullfile(pn.root, 'tools', 'ploterr'); addpath(pn.ploterr);
pn.plotFolder   = fullfile(pn.root, 'figures');
pn.summary = fullfile(pn.root, '..', '..', 'stsw_multimodal', 'data');

load(fullfile(pn.data, 'SummaryData_N102.mat'), 'SummaryData', 'IDs_all');

%SummaryData.EEG.RTs_mean % ID*Attribute*Dimension

filename = fullfile(pn.root, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

% select subjects (multimodal only)
idxMulti_summary = ismember(IDs_all, IDs_EEGMR);
[IDs_all(idxMulti_summary), IDs_EEGMR]

% define YA and OA
idx_YA = cellfun(@str2num, IDs_all)<2000;
idx_OA = cellfun(@str2num, IDs_all)>2000;

%% select best individual L1 feature in each session, separately for EEG and fMRI

acc_merged = squeeze(nanmean(cat(4, SummaryData.EEG.Acc_mean, SummaryData.MRI.Acc_mean),4));
rt_merged = squeeze(nanmean(cat(4, SummaryData.EEG.RTs_md, SummaryData.MRI.RTs_md),4));

dat_ya = squeeze(acc_merged(idxMulti_summary & idx_YA,:,1:4));
dat_oa = squeeze(acc_merged(idxMulti_summary & idx_OA,:,1:4));

rt_ya = squeeze(rt_merged(idxMulti_summary & idx_YA,:,1:4));
rt_oa = squeeze(rt_merged(idxMulti_summary & idx_OA,:,1:4));
    
%% sort by feature performance in L1 (best, second, ...)

for i_sub = 1:size(dat_ya,1)
    [~, idx_feat_ya(i_sub,:)] = sort(dat_ya(i_sub,:,1), 'descend');
    %idx_feat_ya(i_sub,:) = [1,2,3,4];
    data_ya_matched(1:4, i_sub,:) = squeeze(dat_ya(i_sub,idx_feat_ya(i_sub,:),:));
    rt_ya_matched(1:4, i_sub,:) = squeeze(rt_ya(i_sub,idx_feat_ya(i_sub,:),:));
end

for i_sub = 1:size(dat_oa,1)
    [~, idx_feat_oa(i_sub,:)] = sort(dat_oa(i_sub,:,1), 'descend');
    %idx_feat_oa(i_sub,:) = [1,2,3,4];
    data_oa_matched(1:4, i_sub,:) = dat_oa(i_sub,idx_feat_oa(i_sub,:),:);
    rt_oa_matched(1:4, i_sub,:) = squeeze(rt_oa(i_sub,idx_feat_oa(i_sub,:),:));
end

%% only include matched L1 feature conditions (absolute)

h = figure('units','normalized','position',[.1 .1 .15 .25]);
    hold on;
    error = squeeze(nanstd(nanmean(data_ya_matched(1,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(1,:,:),1),2)), error, '--k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(2:4,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(2:4,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(1:3,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:3,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(4,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(4,:,:),1),2)), error, '--r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Mean Accuracy'); xlabel('# of targets');
    %title("Matched features only")
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    
figureName = 'a03s_featuresorted';
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

[h, p] = ttest2(nanmean(data_ya_matched(2:4,:,1),1), nanmean(data_oa_matched(1:3,:,1),1))
[h, p] = ttest2(nanmean(data_ya_matched(2:4,:,2),1), nanmean(data_oa_matched(1:3,:,2),1))
[h, p] = ttest2(nanmean(data_ya_matched(2:4,:,3),1), nanmean(data_oa_matched(1:3,:,3),1))
[h, p] = ttest2(nanmean(data_ya_matched(2:4,:,4),1), nanmean(data_oa_matched(1:3,:,4),1))


%% plot matching RT

h = figure('units','normalized','position',[.1 .1 .15 .25]);
    hold on;
    error = squeeze(nanstd(nanmean(rt_ya_matched(1,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(rt_ya_matched(1,:,:),1),2)), error, '--k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_ya_matched(2:4,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(rt_ya_matched(2:4,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_oa_matched(1:3,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(rt_oa_matched(1:3,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_oa_matched(4,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(rt_oa_matched(4,:,:),1),2)), error, '--r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Md RT'); xlabel('# of targets');
    set(findall(gcf,'-property','FontSize'),'FontSize',18);

%% perform a median split on brainscore within each of the age groups

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'));

% [sortValYA, sortIdxYA] = sort(STSWD_summary.dec_probed.linear_win(idxMulti_summary & idx_YA), 'descend'); % here it is a negative slope
% [sortValOA, sortIdxOA] = sort(STSWD_summary.dec_probed.linear_win(idxMulti_summary & idx_OA), 'descend'); % here it is a negative slope
% [sortValYA, sortIdxYA] = sort(STSWD_summary.SSVEPmag_norm.linear_win(idxMulti_summary & idx_YA), 'descend'); % here it is a negative slope
% [sortValOA, sortIdxOA] = sort(STSWD_summary.SSVEPmag_norm.linear_win(idxMulti_summary & idx_OA), 'descend'); % here it is a negative slope
[sortValYA, sortIdxYA] = sort(squeeze(nanmean(STSWD_summary.SPM_lv1.data(idxMulti_summary & idx_YA,2:4),2))...
    -STSWD_summary.SPM_lv1.data(idxMulti_summary & idx_YA,1), 'descend'); % here it is a negative slope
[sortValOA, sortIdxOA] = sort(squeeze(nanmean(STSWD_summary.SPM_lv1.data(idxMulti_summary & idx_OA,2:4),2))...
    -STSWD_summary.SPM_lv1.data(idxMulti_summary & idx_OA,1), 'descend'); % here it is a negative slope

[sortValYA, sortIdxYA] = sort(squeeze(nanmean(STSWD_summary.SPM_lv2.data(idxMulti_summary & idx_YA,2:4),2))...
    ./STSWD_summary.SPM_lv2.data(idxMulti_summary & idx_YA,1), 'descend'); % here it is a negative slope
[sortValOA, sortIdxOA] = sort(squeeze(nanmean(STSWD_summary.SPM_lv2.data(idxMulti_summary & idx_OA,2:4),2))...
    ./STSWD_summary.SPM_lv2.data(idxMulti_summary & idx_OA,1), 'descend'); % here it is a negative slope

[sortValYA, sortIdxYA] = sort(squeeze(nanmean(STSWD_summary.SPM_lv3.data(idxMulti_summary & idx_YA,2),2))...
    ./STSWD_summary.SPM_lv3.data(idxMulti_summary & idx_YA,1), 'descend'); % here it is a negative slope
[sortValOA, sortIdxOA] = sort(squeeze(nanmean(STSWD_summary.SPM_lv3.data(idxMulti_summary & idx_OA,2),2))...
    ./STSWD_summary.SPM_lv3.data(idxMulti_summary & idx_OA,1), 'descend'); % here it is a negative slope

idx_ya = find(idxMulti_summary & idx_YA);
lowChIdxYA = sortIdxYA(1:floor(numel(sortIdxYA)/3));
midChIdxYA = sortIdxYA(floor(numel(sortIdxYA)/3)+1:2*floor(numel(sortIdxYA)/3));
highChIdxYA = sortIdxYA(2*floor(numel(sortIdxYA)/3)+1:end);

idx_oa = find(idxMulti_summary & idx_OA);
lowChIdxOA = sortIdxOA(1:floor(numel(sortIdxOA)/3));
midChIdxOA = sortIdxOA(floor(numel(sortIdxOA)/3)+1:2*floor(numel(sortIdxOA)/3));
highChIdxOA = sortIdxOA(2*floor(numel(sortIdxOA)/3)+1:end);

h = figure('units','normalized','position',[.1 .1 .15 .25]);
    hold on;
    error = squeeze(nanstd(nanmean(data_ya_matched(1:4,[lowChIdxYA],:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(1:4,[lowChIdxYA],:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(1:4,[lowChIdxOA],:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:4,[lowChIdxOA],:),1),2)), error, 'r', ...
        'linewidth', 2);
    % add high
    error = squeeze(nanstd(nanmean(data_ya_matched(1:4,highChIdxYA,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(1:4,highChIdxYA,:),1),2)), error, '--k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(1:4,highChIdxOA,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:4,highChIdxOA,:),1),2)), error, '--r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Mean Accuracy'); xlabel('# of targets');
    set(findall(gcf,'-property','FontSize'),'FontSize',18);

h = figure('units','normalized','position',[.1 .1 .15 .25]);
    hold on;
    error = squeeze(nanstd(nanmean(data_ya_matched(2:4,[lowChIdxYA],:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(2:4,[lowChIdxYA],:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(1:3,[lowChIdxOA],:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:3,[lowChIdxOA],:),1),2)), error, 'r', ...
        'linewidth', 2);
    % add high
    error = squeeze(nanstd(nanmean(data_ya_matched(2:4,highChIdxYA,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(2:4,highChIdxYA,:),1),2)), error, '--k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(1:3,highChIdxOA,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:3,highChIdxOA,:),1),2)), error, '--r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Mean Accuracy'); xlabel('# of targets');
    set(findall(gcf,'-property','FontSize'),'FontSize',18);

h = figure('units','normalized','position',[.1 .1 .15 .25]);
    hold on;
    curData = STSWD_summary.SPM_lv1.data;
%    curData = STSWD_summary.HDDM_vat_matched.driftEEGMRI_pc;
    error = squeeze(nanstd(curData([idx_ya(lowChIdxYA)],:),[],1))./sqrt(42);
    errorbar(squeeze(nanmean(curData([idx_ya(lowChIdxYA)],:),1)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(curData([idx_oa(lowChIdxOA)],:),[],1))./sqrt(53);
    errorbar(squeeze(nanmean(curData([idx_oa(lowChIdxOA)],:),1)), error, 'r', ...
        'linewidth', 2);
    % add high
    error = squeeze(nanstd(curData(idx_ya(highChIdxYA),:),[],1))./sqrt(42);
    errorbar(squeeze(nanmean(curData(idx_ya(highChIdxYA),:),1)), error, '--k', ...
        'linewidth', 2);
    error = squeeze(nanstd(curData(idx_oa(highChIdxOA),:),[],1))./sqrt(53);
    errorbar(squeeze(nanmean(curData(idx_oa(highChIdxOA),:),1)), error, '--r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    xlabel('# of targets');
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    
    h = figure('units','normalized','position',[.1 .1 .15 .25]);
    hold on;
    curData = STSWD_summary.HDDM_vat.driftEEGMRI;
    error = squeeze(nanstd(curData([idx_ya(midChIdxYA)],:),[],1))./sqrt(42);
    errorbar(squeeze(nanmean(curData([idx_ya(midChIdxYA)],:),1)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(curData([idx_oa(midChIdxOA)],:),[],1))./sqrt(53);
    errorbar(squeeze(nanmean(curData([idx_oa(midChIdxOA)],:),1)), error, 'r', ...
        'linewidth', 2);
    % add high
    error = squeeze(nanstd(curData(idx_ya(highChIdxYA),:),[],1))./sqrt(42);
    errorbar(squeeze(nanmean(curData(idx_ya(highChIdxYA),:),1)), error, '--k', ...
        'linewidth', 2);
    error = squeeze(nanstd(curData(idx_oa(highChIdxOA),:),[],1))./sqrt(53);
    errorbar(squeeze(nanmean(curData(idx_oa(highChIdxOA),:),1)), error, '--r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    xlabel('# of targets');
    set(findall(gcf,'-property','FontSize'),'FontSize',18);

    
%%

h = figure('units','normalized','position',[.1 .1 .15 .25]);
    hold on;
    error = squeeze(nanstd(nanmean(data_ya_matched(1,lowChIdxYA,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(1,lowChIdxYA,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(2:4,lowChIdxYA,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(2:4,lowChIdxYA,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(1:3,lowChIdxOA,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:3,lowChIdxOA,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(4,lowChIdxOA,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(4,lowChIdxOA,:),1),2)), error, 'r', ...
        'linewidth', 2);
    % add mid
    error = squeeze(nanstd(nanmean(data_ya_matched(1,midChIdxYA,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(1,midChIdxYA,:),1),2)), error, ':k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(2:4,midChIdxYA,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(2:4,midChIdxYA,:),1),2)), error, ':k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(1:3,midChIdxOA,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:3,midChIdxOA,:),1),2)), error, ':r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(4,midChIdxOA,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(4,midChIdxOA,:),1),2)), error, ':r', ...
        'linewidth', 2);
    % add high
    error = squeeze(nanstd(nanmean(data_ya_matched(1,highChIdxYA,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(1,highChIdxYA,:),1),2)), error, '--k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(2:4,highChIdxYA,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(2:4,highChIdxYA,:),1),2)), error, '--k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(1:3,highChIdxOA,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:3,highChIdxOA,:),1),2)), error, '--r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(4,highChIdxOA,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(4,highChIdxOA,:),1),2)), error, '--r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Mean Accuracy'); xlabel('# of targets');
    set(findall(gcf,'-property','FontSize'),'FontSize',18);

h = figure('units','normalized','position',[.1 .1 .15 .25]);
    hold on;
    error = squeeze(nanstd(nanmean(rt_ya_matched(1,lowChIdxYA,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(rt_ya_matched(1,lowChIdxYA,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_ya_matched(2:4,lowChIdxYA,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(rt_ya_matched(2:4,lowChIdxYA,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_oa_matched(1:3,lowChIdxOA,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(rt_oa_matched(1:3,lowChIdxOA,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_oa_matched(4,lowChIdxOA,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(rt_oa_matched(4,lowChIdxOA,:),1),2)), error, 'r', ...
        'linewidth', 2);
    % add high
    error = squeeze(nanstd(nanmean(rt_ya_matched(1,highChIdxYA,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(rt_ya_matched(1,highChIdxYA,:),1),2)), error, '--k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_ya_matched(2:4,highChIdxYA,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(rt_ya_matched(2:4,highChIdxYA,:),1),2)), error, '--k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_oa_matched(1:3,highChIdxOA,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(rt_oa_matched(1:3,highChIdxOA,:),1),2)), error, '--r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(rt_oa_matched(4,highChIdxOA,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(rt_oa_matched(4,highChIdxOA,:),1),2)), error, '--r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Md RT'); xlabel('# of targets');
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    
%% plot raw

figure;
subplot(1,2,1);
    hold on;
    error = squeeze(nanstd(nanmean(data_ya_matched(1,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(1,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(2,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(2,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(3,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(3,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_matched(4,:,:),1),[],2))./sqrt(42);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched(4,:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylim([0.5 1]);
    ylabel('Mean Accuracy'); xlabel('# of targets');
    %ylim([-0.2 .05]);
    title('YA')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
subplot(1,2,2);
    hold on;
    error = squeeze(nanstd(nanmean(data_oa_matched(1,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(1,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(2,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(2,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(3,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(3,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched(4,:,:),1),[],2))./sqrt(53);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched(4,:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Mean Accuracy'); xlabel('# of targets');
    ylim([0.5 1]);
    title('OA')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    suptitle('sorted by feature accuracy L1')

    
%% plot median split for LV1

[sortValYA, sortIdxYA] = sort(squeeze(nanmean(STSWD_summary.SPM_lv1.data(idxMulti_summary & idx_YA,2:4),2))...
    -STSWD_summary.SPM_lv1.data(idxMulti_summary & idx_YA,1), 'descend'); % here it is a negative slope
[sortValOA, sortIdxOA] = sort(squeeze(nanmean(STSWD_summary.SPM_lv1.data(idxMulti_summary & idx_OA,2:4),2))...
    -STSWD_summary.SPM_lv1.data(idxMulti_summary & idx_OA,1), 'descend'); % here it is a negative slope

idx_ya = find(idxMulti_summary & idx_YA);
lowChIdxYA = sortIdxYA(1:floor(numel(sortIdxYA)/3));
midChIdxYA = sortIdxYA(floor(numel(sortIdxYA)/3)+1:2*floor(numel(sortIdxYA)/3));
highChIdxYA = sortIdxYA(2*floor(numel(sortIdxYA)/3)+1:end);

idx_oa = find(idxMulti_summary & idx_OA);
lowChIdxOA = sortIdxOA(1:floor(numel(sortIdxOA)/3));
midChIdxOA = sortIdxOA(floor(numel(sortIdxOA)/3)+1:2*floor(numel(sortIdxOA)/3));
highChIdxOA = sortIdxOA(2*floor(numel(sortIdxOA)/3)+1:end);

% h = figure('units','normalized','position',[.1 .1 .15 .25]);
%     hold on;
%     error = squeeze(nanstd(nanmean(data_ya_matched(1:4,[lowChIdxYA],:),1),[],2))./sqrt(42);
%     errorbar(squeeze(nanmean(nanmean(data_ya_matched(1:4,[lowChIdxYA],:),1),2)), error, 'k', ...
%         'linewidth', 2);
%     error = squeeze(nanstd(nanmean(data_oa_matched(1:4,[lowChIdxOA],:),1),[],2))./sqrt(53);
%     errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:4,[lowChIdxOA],:),1),2)), error, 'r', ...
%         'linewidth', 2);
%     % add high
%     error = squeeze(nanstd(nanmean(data_ya_matched(1:4,highChIdxYA,:),1),[],2))./sqrt(42);
%     errorbar(squeeze(nanmean(nanmean(data_ya_matched(1:4,highChIdxYA,:),1),2)), error, '--k', ...
%         'linewidth', 2);
%     error = squeeze(nanstd(nanmean(data_oa_matched(1:4,highChIdxOA,:),1),[],2))./sqrt(53);
%     errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:4,highChIdxOA,:),1),2)), error, '--r', ...
%         'linewidth', 2);
%     set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
%             'xlim', [0.5 4.5]);
%     ylabel('Mean Accuracy'); xlabel('# of targets');
%     set(findall(gcf,'-property','FontSize'),'FontSize',18);
% 
% h = figure('units','normalized','position',[.1 .1 .15 .25]);
%     hold on;
%     error = squeeze(nanstd(nanmean(data_ya_matched(2:4,[lowChIdxYA],:),1),[],2))./sqrt(42);
%     errorbar(squeeze(nanmean(nanmean(data_ya_matched(2:4,[lowChIdxYA],:),1),2)), error, 'k', ...
%         'linewidth', 2);
%     error = squeeze(nanstd(nanmean(data_oa_matched(1:3,[lowChIdxOA],:),1),[],2))./sqrt(53);
%     errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:3,[lowChIdxOA],:),1),2)), error, 'r', ...
%         'linewidth', 2);
%     % add high
%     error = squeeze(nanstd(nanmean(data_ya_matched(2:4,highChIdxYA,:),1),[],2))./sqrt(42);
%     errorbar(squeeze(nanmean(nanmean(data_ya_matched(2:4,highChIdxYA,:),1),2)), error, '--k', ...
%         'linewidth', 2);
%     error = squeeze(nanstd(nanmean(data_oa_matched(1:3,highChIdxOA,:),1),[],2))./sqrt(53);
%     errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:3,highChIdxOA,:),1),2)), error, '--r', ...
%         'linewidth', 2);
%     set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
%             'xlim', [0.5 4.5]);
%     ylabel('Mean Accuracy'); xlabel('# of targets');
%     set(findall(gcf,'-property','FontSize'),'FontSize',18);

h = figure('units','normalized','position',[.1 .1 .15*3 .25]);
subplot(1,3,1);
    hold on;
    curData = STSWD_summary.SPM_lv1.data;
    idx = [idx_ya(lowChIdxYA); idx_oa(lowChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'k', ...
        'linewidth', 2);
    % add high
    idx = [idx_ya(highChIdxYA);idx_oa(highChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--k', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylim([-1.5 3]*10^4);
    xlabel('# of targets');
    ylabel('BOLD LV1')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
subplot(1,3,2);
    hold on;
    curData = squeeze(nanmean(cat(3, STSWD_summary.behav.EEGAcc, STSWD_summary.behav.MRIAcc),3));
    idx = [idx_ya(lowChIdxYA); idx_oa(lowChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'k', ...
        'linewidth', 2);
    % add high
    idx = [idx_ya(highChIdxYA);idx_oa(highChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--k', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylim([.5 1]);
    xlabel('# of targets');
    ylabel('Accuracy')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    
subplot(1,3,3);
    hold on;
    curData = STSWD_summary.HDDM_vat.driftEEGMRI;
    idx = [idx_ya(lowChIdxYA); idx_oa(lowChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'k', ...
        'linewidth', 2);
    % add high
    idx = [idx_ya(highChIdxYA);idx_oa(highChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--k', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylim([0.25 2]);
    xlabel('# of targets');
    ylabel('Drift rate')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    
figureName = 'a03s_mediansplit';
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

h = []; p =[];
curData = STSWD_summary.SPM_lv1.data;
for indCond = 1:4
    [h(indCond),p(indCond)] = ttest2(curData([idx_ya(lowChIdxYA); idx_oa(lowChIdxOA)],indCond),...
        curData([idx_ya(highChIdxYA);idx_oa(highChIdxOA)],indCond));
end

h = []; p =[];
curData = squeeze(nanmean(cat(3, STSWD_summary.behav.EEGAcc, STSWD_summary.behav.MRIAcc),3));
for indCond = 1:4
    [h(indCond),p(indCond)] = ttest2(curData([idx_ya(lowChIdxYA); idx_oa(lowChIdxOA)],indCond),...
        curData([idx_ya(highChIdxYA);idx_oa(highChIdxOA)],indCond));
end

h = []; p =[];
curData = STSWD_summary.HDDM_vat.driftEEGMRI;
for indCond = 1:4
    [h(indCond),p(indCond)] = ttest2(curData([idx_ya(lowChIdxYA); idx_oa(lowChIdxOA)],indCond),...
        curData([idx_ya(highChIdxYA);idx_oa(highChIdxOA)],indCond));
end

%% for YA and OA

h = figure('units','normalized','position',[.1 .1 .15*3 .2]);
subplot(1,4,1);
    hold on;
    curData = STSWD_summary.SPM_lv1.data;
    idx = [idx_ya(lowChIdxYA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'k', ...
        'linewidth', 2);
    % add high
    idx = [idx_ya(highChIdxYA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--k', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylim([-1.5 3]*10^4);
    xlabel('# of targets');
    ylabel('BOLD LV1')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
subplot(1,4,2);
    hold on;
    curData = STSWD_summary.SPM_lv1.data;
    idx = [idx_oa(lowChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'r', ...
        'linewidth', 2);
    % add high
    idx = [idx_oa(highChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylim([-1.5 3]*10^4);
    xlabel('# of targets');
    ylabel('BOLD LV1')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
subplot(1,4,3);
    hold on;
    curData = squeeze(nanmean(cat(3, STSWD_summary.behav.EEGAcc, STSWD_summary.behav.MRIAcc),3));
    idx = [idx_ya(lowChIdxYA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'k', ...
        'linewidth', 2);
    % add high
    idx = [idx_ya(highChIdxYA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--k', ...
        'linewidth', 2);
    idx = [idx_oa(lowChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'r', ...
        'linewidth', 2);
    % add high
    idx = [idx_oa(highChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylim([.5 1]);
    xlabel('# of targets');
    ylabel('Accuracy')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
subplot(1,4,4);
    hold on;
    curData = STSWD_summary.HDDM_vat.driftEEGMRI;
    idx = [idx_ya(lowChIdxYA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'k', ...
        'linewidth', 2);
    % add high
    idx = [idx_ya(highChIdxYA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--k', ...
        'linewidth', 2);
    
    idx = [idx_oa(lowChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'r', ...
        'linewidth', 2);
    % add high
    idx = [idx_oa(highChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    %ylim([0.25 2]);
    xlabel('# of targets');
    ylabel('Drift rate')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);

figureName = 'a03s_mediansplit_yaoa';
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');