clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data         = fullfile(pn.root, 'data');
    pn.tools    = fullfile(pn.root, 'tools'); addpath(pn.tools);
    pn.bars     = fullfile(pn.root, 'tools', 'barwitherr'); addpath(pn.bars);
    pn.brewer   = fullfile(pn.root, 'tools', 'BrewerMap'); addpath(pn.brewer);
    pn.ploterr  = fullfile(pn.root, 'tools', 'ploterr'); addpath(pn.ploterr);
pn.plotFolder   = fullfile(pn.root, 'figures');

load(fullfile(pn.data, 'SummaryData_N102.mat'), 'SummaryData', 'IDs_all');

%SummaryData.EEG.RTs_mean % ID*Attribute*Dimension

filename = fullfile(pn.root, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

% select subjects (multimodal only)
idxMulti_summary = ismember(IDs_all, IDs_EEGMR);
[IDs_all(idxMulti_summary), IDs_EEGMR]

% define YA and OA
idx_YA = cellfun(@str2num, IDs_all)<2000;
idx_OA = cellfun(@str2num, IDs_all)>2000;

%% average across sessions

acc_merged = squeeze(nanmean(cat(4, SummaryData.EEG.Acc_mean),4));

%% get avg. accuracy across conditions for each attribute

attributes = {'Color','Direction','Size', 'Luminance'};

colorm = [230/265 25/265 25/265; 0/265 50/265 100/265]; % use external function to create colormap

h = figure('units','normalized','position',[.1 .1 .15 .2]);
    set(0, 'DefaultFigureRenderer', 'painters');
    dat_ya = squeeze(nanmean(acc_merged(idxMulti_summary & idx_YA,[1,3,4],1:4),2));
    dat_oa = squeeze(nanmean(acc_merged(idxMulti_summary & idx_OA,[1,3,4],1:4),2));

    hold on;
    % if we want each bar to have a different color, loop
    for b = 1:size(dat_ya, 2)
        bar(b-.2, nanmean(dat_ya(:,b)), 'FaceColor',  [1 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
        bar(b+.2, nanmean(dat_oa(:,b)), 'FaceColor',  [.8 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    end
    hold on; line([0 5], [.5 .5], 'Color', 'k', 'LineStyle', '--', 'LineWidth', 2)
    
    % plot jittered individual values on top
    scatter(repmat(1-.2,size(dat_ya,1),1)+(rand(size(dat_ya,1),1)-.5).*.4, dat_ya(:,1), 20, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    scatter(repmat(2-.2,size(dat_ya,1),1)+(rand(size(dat_ya,1),1)-.5).*.4, dat_ya(:,2), 20, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    scatter(repmat(3-.2,size(dat_ya,1),1)+(rand(size(dat_ya,1),1)-.5).*.4, dat_ya(:,3), 20, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    scatter(repmat(4-.2,size(dat_ya,1),1)+(rand(size(dat_ya,1),1)-.5).*.4, dat_ya(:,4), 20, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    
    scatter(repmat(1+.2,size(dat_oa,1),1)+(rand(size(dat_oa,1),1)-.5).*.4, dat_oa(:,1), 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    scatter(repmat(2+.2,size(dat_oa,1),1)+(rand(size(dat_oa,1),1)-.5).*.4, dat_oa(:,2), 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    scatter(repmat(3+.2,size(dat_oa,1),1)+(rand(size(dat_oa,1),1)-.5).*.4, dat_oa(:,3), 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    scatter(repmat(4+.2,size(dat_oa,1),1)+(rand(size(dat_oa,1),1)-.5).*.4, dat_oa(:,4), 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    
%     % show standard error on top
%     h1 = ploterr([1:size(dat_ya,2)]-.2, nanmean(dat_ya,1), [], nanstd(dat_ya,[],1)./sqrt(size(dat_ya,1)), 'k.', 'abshhxy', 0);
%     set(h1(1), 'marker', 'none'); % remove marker
%     set(h1(2), 'LineWidth', 4);
%     h2 = ploterr([1:size(dat_oa,2)]+.2, nanmean(dat_oa,1), [], nanstd(dat_oa,[],1)./sqrt(size(dat_oa,1)), 'k.', 'abshhxy', 0);
%     set(h2(1), 'marker', 'none'); % remove marker
%     set(h2(2), 'LineWidth', 4);

    % label what we're seeing
    % if labels are too long to fit, use the xticklabelrotation with about -30
    % to rotate them so they're readable
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', attributes, ...
        'xlim', [0.5 4.5]); ylim([0 1])
    ylabel('Mean Accuracy'); %xlabel('# of targets');

    % if these data are paired, show the differences
    % plot(dat_ya', '.k-', 'linewidth', 0.2, 'markersize', 2);

    for indCond = 1:4
        % significance star for the difference
        [~, pval] = ttest(dat_ya(:, indCond), repmat(.5,size(dat_ya(:, indCond)))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        mysigstar(gca, [indCond]-.2, .35, pval);
        
        % significance star for the difference
        [~, pval] = ttest(dat_oa(:, indCond), repmat(.5,size(dat_oa(:, indCond)))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        mysigstar(gca, [indCond]+.2, .35, pval);
    end
    
    ylim([.3 1])
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
% figureName = 'E2_vsGuess_yaoa';
% saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
% saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% compare similar performance level starting points for younger and older adults

% select only EEG results for accuracy
acc_merged = squeeze(nanmean(cat(4, SummaryData.EEG.Acc_mean),4));

attributes = {'1','2:4'};

colorm = [230/265 25/265 25/265; 0/265 50/265 100/265]; % use external function to create colormap

h = figure('units','normalized','position',[.1 .1 .15 .2]);
    set(0, 'DefaultFigureRenderer', 'painters');
    dat_ya = squeeze(nanmean(acc_merged(idxMulti_summary & idx_YA,[2],1:4),2));
    dat_oa = squeeze(nanmean(acc_merged(idxMulti_summary & idx_OA,[2],1:4),2));

    hold on;

    bar(1-.2, nanmean(dat_ya(:,1)), 'FaceColor',  [1 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    bar(1+.2, nanmean(dat_oa(:,1)), 'FaceColor',  [.8 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    bar(2-.2, nanmean(nanmean(dat_ya(:,2:4),2),1), 'FaceColor',  [1 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    bar(2+.2, nanmean(nanmean(dat_oa(:,2:4),2),1), 'FaceColor',  [.8 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    hold on; line([0 3], [.5 .5], 'Color', 'k', 'LineStyle', '--', 'LineWidth', 2)
    
    % plot jittered individual values on top
%     scatter(repmat(1-.2,size(dat_ya,1),1)+(rand(size(dat_ya,1),1)-.5).*.4, dat_ya(:,1), 20, 'filled', 'MarkerFaceColor', [1 .3 .3]);
%     scatter(repmat(2-.2,size(dat_ya,1),1)+(rand(size(dat_ya,1),1)-.5).*.4, nanmean(dat_ya(:,2:4),2), 20, 'filled', 'MarkerFaceColor', [1 .3 .3]);
%     
%     scatter(repmat(1+.2,size(dat_oa,1),1)+(rand(size(dat_oa,1),1)-.5).*.4, dat_oa(:,1), 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
%     scatter(repmat(2+.2,size(dat_oa,1),1)+(rand(size(dat_oa,1),1)-.5).*.4, nanmean(dat_oa(:,2:4),2), 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
%     
    % show standard error on top
    h1 = ploterr([1:size(dat_ya,2)]-.2, nanmean(dat_ya,1), [], nanstd(dat_ya,[],1)./sqrt(size(dat_ya,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    h2 = ploterr([1:size(dat_oa,2)]+.2, nanmean(dat_oa,1), [], nanstd(dat_oa,[],1)./sqrt(size(dat_oa,1)), 'k.', 'abshhxy', 0);
    set(h2(1), 'marker', 'none'); % remove marker
    set(h2(2), 'LineWidth', 4);

    % label what we're seeing
    % if labels are too long to fit, use the xticklabelrotation with about -30
    % to rotate them so they're readable
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', attributes, ...
        'xlim', [0.5 4.5]); ylim([0 1])
    ylabel('Mean Accuracy'); %xlabel('# of targets');

    % if these data are paired, show the differences
    % plot(dat_ya', '.k-', 'linewidth', 0.2, 'markersize', 2);

%     [h, p] = ttest(dat_ya(:,1), nanmean(dat_ya(:,2:4),2))
%     [h, p] = ttest(dat_oa(:,1), nanmean(dat_oa(:,2:4),2))
%     
%     [h, p] = ttest2(dat_ya(:,1), dat_oa(:,1))
%     [h, p] = ttest2(nanmean(dat_ya(:,2:4),2), nanmean(dat_oa(:,2:4),2))
    
    % significance star
    [h, pval] = ttest2(dat_ya(:,1), dat_oa(:,1))
    mysigstar(gca, [1]-.2, .75, pval);

    % significance star for the difference
    [h, pval] = ttest2(nanmean(dat_ya(:,2:4),2), nanmean(dat_oa(:,2:4),2))
    % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
    % sigstars on top
    mysigstar(gca, [2]+.2, .75, pval);
    
    %ylim([.7 .95])
    xlim([0.5 2.5])
    title("EEG: direction")
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
