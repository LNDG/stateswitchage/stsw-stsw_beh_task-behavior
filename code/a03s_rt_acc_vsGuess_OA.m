clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data         = fullfile(pn.root, 'data');
pn.bars         = fullfile(pn.root, 'tools', 'barwitherr'); addpath(pn.bars);
pn.brewer       = fullfile(pn.root, 'tools', 'BrewerMap'); addpath(pn.brewer);
pn.plotFolder   = fullfile(pn.root, 'figures');

load(fullfile(pn.data, 'SummaryData_N102.mat'), 'SummaryData', 'IDs_all');

%SummaryData.EEG.RTs_mean % ID*Attribute*Dimension

%% select only young adults in final EEG sample

% N = 53;
IDs_OA = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

% select subjects (EEG only)
idxEEG_summary = ismember(IDs_all, IDs_OA);
[IDs_all(idxEEG_summary), IDs_OA]

% N = 53;
IDs_OA_EEG_MR = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

% select subjects (EEG-MR only)
idxMR_summary = ismember(IDs_all, IDs_OA_EEG_MR);
[IDs_all(idxMR_summary), IDs_OA_EEG_MR]

%% get load 1 accuracy for each attribute

attributes = {'Color','Direction','Size', 'Luminance'};

colorm = [230/265 25/265 25/265; 0/265 50/265 100/265]; % use external function to create colormap

h = figure('units','normalized','position',[.1 .1 .4 .2]);

subplot(1,2,1); cla;
    
    indLoad = 1;
    dat = squeeze(SummaryData.EEG.Acc_mean(idxEEG_summary,1:4,indLoad)); % select only load 1 for each target, only accuracy here
    dat4 = squeeze(SummaryData.EEG.Acc_mean(idxEEG_summary,1:4,4)); % select only load 1 for each target, only accuracy here

    hold on;
    % if we want each bar to have a different color, loop
    for b = 1:size(dat, 2)
        bar(b-.2, nanmean(dat(:,b)), 'FaceColor',  [1 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
        bar(b+.2, nanmean(dat4(:,b)), 'FaceColor',  [.8 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    end
    hold on; line([0 5], [.5 .5], 'Color', 'k', 'LineStyle', '--', 'LineWidth', 2)
    
    % plot jittered individual values on top
    scatter(repmat(1-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,1), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    scatter(repmat(2-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,2), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    scatter(repmat(3-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,3), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    scatter(repmat(4-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,4), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    
    scatter(repmat(1+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,1), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    scatter(repmat(2+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,2), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    scatter(repmat(3+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,3), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    scatter(repmat(4+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,4), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    
    % show standard error on top
    h1 = ploterr([1:size(dat,2)]-.2, nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    h2 = ploterr([1:size(dat4,2)]+.2, nanmean(dat4,1), [], nanstd(dat4,[],1)./sqrt(size(dat4,1)), 'k.', 'abshhxy', 0);
    set(h2(1), 'marker', 'none'); % remove marker
    set(h2(2), 'LineWidth', 4);

    % label what we're seeing
    % if labels are too long to fit, use the xticklabelrotation with about -30
    % to rotate them so they're readable
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', attributes, ...
        'xlim', [0.5 4.5]); ylim([0 1])
    ylabel('Mean Accuracy'); %xlabel('# of targets');

    % if these data are paired, show the differences
    % plot(dat', '.k-', 'linewidth', 0.2, 'markersize', 2);

    for indCond = 1:4
        % significance star for the difference
        [~, pval] = ttest(dat(:, indCond), repmat(.5,size(dat(:, indCond)))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        mysigstar(gca, [indCond]-.2, .1, pval);
        
        % significance star for the difference
        [~, pval] = ttest(dat4(:, indCond), repmat(.5,size(dat4(:, indCond)))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        mysigstar(gca, [indCond]+.2, .1, pval);
    end
    title('EEG session');
    
subplot(1,2,2); cla; hold on;

    dat = squeeze(SummaryData.MRI.Acc_mean(idxMR_summary,1:4,indLoad)); % select only load 1 for each target, only accuracy here
    dat4 = squeeze(SummaryData.MRI.Acc_mean(idxMR_summary,1:4,4)); % select only load 1 for each target, only accuracy here

     hold on;
    % if we want each bar to have a different color, loop
    for b = 1:size(dat, 2)
        bar(b-.2, nanmean(dat(:,b)), 'FaceColor',  [1 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
        bar(b+.2, nanmean(dat4(:,b)), 'FaceColor', [.8 .8 .8], 'EdgeColor', 'none', 'BarWidth', 0.4);
    end
    hold on; line([0 5], [.5 .5], 'Color', 'k', 'LineStyle', '--', 'LineWidth', 2)
    
    % plot jittered individual values on top
    scatter(repmat(1-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,1), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    scatter(repmat(2-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,2), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    scatter(repmat(3-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,3), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    scatter(repmat(4-.2,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,4), 10, 'filled', 'MarkerFaceColor', [1 .3 .3]);
    
    scatter(repmat(1+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,1), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    scatter(repmat(2+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,2), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    scatter(repmat(3+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,3), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    scatter(repmat(4+.2,size(dat,1),1)+(rand(size(dat4,1),1)-.5).*.4, dat4(:,4), 10, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    
    % show standard error on top
    h1 = ploterr([1:size(dat,2)]-.2, nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    h2 = ploterr([1:size(dat4,2)]+.2, nanmean(dat4,1), [], nanstd(dat4,[],1)./sqrt(size(dat4,1)), 'k.', 'abshhxy', 0);
    set(h2(1), 'marker', 'none'); % remove marker
    set(h2(2), 'LineWidth', 4);

    % label what we're seeing
    % if labels are too long to fit, use the xticklabelrotation with about -30
    % to rotate them so they're readable
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', attributes, ...
        'xlim', [0.5 4.5]); ylim([0 1])
    ylabel('Mean Accuracy'); %xlabel('# of targets');

    % if these data are paired, show the differences
    % plot(dat', '.k-', 'linewidth', 0.2, 'markersize', 2);

    for indCond = 1:4
        % significance star for the difference
        [~, pval] = ttest(dat(:, indCond), repmat(.5,size(dat(:, indCond)))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        mysigstar(gca, [indCond]-.2, .1, pval);
        
        % significance star for the difference
        [~, pval] = ttest(dat4(:, indCond), repmat(.5,size(dat4(:, indCond)))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        mysigstar(gca, [indCond]+.2, .1, pval);
    end
    
    title('MRI session');

set(findall(gcf,'-property','FontSize'),'FontSize',20)
    
figureName = 'E2_vsGuess_OA';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% SourceData

SourceData_EEG1 = squeeze(SummaryData.EEG.Acc_mean(idxEEG_summary,1:4,1));
SourceData_EEG4 = squeeze(SummaryData.EEG.Acc_mean(idxEEG_summary,1:4,4));
SourceData_MRI1 = squeeze(SummaryData.MRI.Acc_mean(idxMR_summary,1:4,1));
SourceData_MRI4 = squeeze(SummaryData.MRI.Acc_mean(idxMR_summary,1:4,4));
