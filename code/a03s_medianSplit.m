clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data         = fullfile(pn.root, 'data');
    pn.tools    = fullfile(pn.root, 'tools'); addpath(pn.tools);
    pn.bars     = fullfile(pn.root, 'tools', 'barwitherr'); addpath(pn.bars);
    pn.brewer   = fullfile(pn.root, 'tools', 'BrewerMap'); addpath(pn.brewer);
    pn.ploterr  = fullfile(pn.root, 'tools', 'ploterr'); addpath(pn.ploterr);
pn.plotFolder   = fullfile(pn.root, 'figures');

load(fullfile(pn.data, 'SummaryData_N102.mat'), 'SummaryData', 'IDs_all');

%SummaryData.EEG.RTs_mean % ID*Attribute*Dimension

filename = fullfile(pn.root, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

% select subjects (multimodal only)
idxMulti_summary = ismember(IDs_all, IDs_EEGMR);
[IDs_all(idxMulti_summary), IDs_EEGMR]

% define YA and OA
idx_YA = cellfun(@str2num, IDs_all)<2000;
idx_OA = cellfun(@str2num, IDs_all)>2000;

%% median splitting

acc_merged = squeeze(nanmean(cat(4, SummaryData.EEG.Acc_mean, SummaryData.MRI.Acc_mean),4));

for indAtt = 1:4
    dat_ya = squeeze(nanmean(acc_merged(idxMulti_summary & idx_YA,indAtt,1:4),2));
    dat_oa = squeeze(nanmean(acc_merged(idxMulti_summary & idx_OA,indAtt,1:4),2));
    
    [sortval_ya, sortind_ya] = sort(dat_ya(:,1), 'descend');
    [sortval_oa, sortind_oa] = sort(dat_oa(:,1), 'descend');
    
    N_ya = numel(sortind_ya);
    N_oa = numel(sortind_oa);
    
    data_ya_matched(indAtt, :,:) = dat_ya; data_ya_matched(indAtt, :,:) = NaN;
    data_ya_matched(indAtt, 1:floor(N_ya/2),:) = dat_ya(sortind_ya(1:floor(N_ya/2)),:);
    data_ya_unmatched(indAtt, :,:) = dat_ya; data_ya_unmatched(indAtt, :,:) = NaN;
    data_ya_unmatched(indAtt, floor(N_ya/2)+1:end,:) = dat_ya(sortind_ya(floor(N_ya/2)+1:end),:);
    
    data_oa_matched(indAtt, :,:) = dat_oa; data_oa_matched(indAtt, :,:) = NaN;
    data_oa_matched(indAtt, 1:floor(N_oa/2),:) = dat_oa(sortind_oa(1:floor(N_oa/2)),:);
    data_oa_unmatched(indAtt, :,:) = dat_oa; data_oa_unmatched(indAtt, :,:) = NaN;
    data_oa_unmatched(indAtt, floor(N_oa/2)+1:end,:) = dat_oa(sortind_oa(floor(N_oa/2)+1:end),:);    
end

figure;
subplot(1,2,1);
    hold on;
    error = squeeze(nanstd(nanmean(data_ya_matched([1,3,4],:,:),1),[],2))./sqrt(21);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched([1,3,4],:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched([1,3,4],:,:),1),[],2))./sqrt(26);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched([1,3,4],:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_unmatched([1,3,4],:,:),1),[],2))./sqrt(21);
    errorbar(squeeze(nanmean(nanmean(data_ya_unmatched([1,3,4],:,:),1),2)), error, 'k--', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_unmatched([1,3,4],:,:),1),[],2))./sqrt(26);
    errorbar(squeeze(nanmean(nanmean(data_oa_unmatched([1,3,4],:,:),1),2)), error, 'r--', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]); ylim([0 1])
    ylabel('Mean Accuracy'); xlabel('# of targets');
    ylim([.5 1]);
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    legend({'YA top'; 'OA top'; 'YA bottom'; 'OA bottom'});
    title("All excl. Motion Direction")
subplot(1,2,2);
    hold on;
    error = squeeze(nanstd(nanmean(data_ya_matched([2],:,:),1),[],2))./sqrt(21);
    errorbar(squeeze(nanmean(nanmean(data_ya_matched([2],:,:),1),2)), error, 'k', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_matched([2],:,:),1),[],2))./sqrt(26);
    errorbar(squeeze(nanmean(nanmean(data_oa_matched([2],:,:),1),2)), error, 'r', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_ya_unmatched([2],:,:),1),[],2))./sqrt(21);
    errorbar(squeeze(nanmean(nanmean(data_ya_unmatched([2],:,:),1),2)), error, 'k--', ...
        'linewidth', 2);
    error = squeeze(nanstd(nanmean(data_oa_unmatched([2],:,:),1),[],2))./sqrt(26);
    errorbar(squeeze(nanmean(nanmean(data_oa_unmatched([2],:,:),1),2)), error, 'r--', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]); ylim([0 1])
    ylabel('Mean Accuracy'); xlabel('# of targets');
    ylim([.5 1]);
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    legend({'YA top'; 'OA top'; 'YA bottom'; 'OA bottom'});
    title("Motion Direction")