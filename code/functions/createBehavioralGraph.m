function [stats, handle,h] = createBehavioralGraph(dataFullX,dataFullY,colorm, h,conditions,IDSelect)
    
    for indCond = 1:numel(conditions)
        dataX = dataFullX(eval(conditions{indCond}),1);
        dataY = dataFullY(eval(conditions{indCond}),1);
        scatter(dataX, dataY,40, colorm(indCond,:),'filled');
        [rho p] = corr(dataX, dataY, 'type', 'Pearson', 'rows', 'complete');
        scientificVals = cell(size(p));
        for indP = 1:numel(p)
            if p(indP) < 10^-3
                scientificVals{indP} = sprintf('%.0d',p(indP));
            else
                scientificVals{indP} = num2str(round(p(indP),3));
            end
        end
        stats.rho_byCond{indCond} = rho;
        stats.scientificVals_byCond{indCond} = scientificVals{1,1};
        Fit = polyfit(dataX,dataY,1);
        hold on; handle{indCond} = plot(dataX, polyval(Fit,dataX),'Color', colorm(indCond,:), 'LineWidth', 3);
    end
    
end