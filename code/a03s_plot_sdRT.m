currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.root = rootpath;
pn.tools = fullfile(pn.root, 'tools');
pn.plotFolder = fullfile(pn.root, 'figures');
pn.dataOut = fullfile(pn.root, '..','..','stsw_multimodal','data');
load(fullfile(pn.dataOut, 'STSWD_summary_YAOA.mat'), 'STSWD_summary');

idx_YA = cellfun(@str2num, STSWD_summary.IDs)<2000;
idx_OA = cellfun(@str2num, STSWD_summary.IDs)>2000;

%% plot mean effects using RainCloudPlots
 
addpath(genpath(pn.tools))

lims{1} = [0 0.4]; lims{2} = [0 0.4];
colorm = [2.*[.3 .1 .1]; 2.*[.3 .1 .1]; 0.0314, 0.3176, 0.6118; 0.0314, 0.3176, 0.6118]
paramLabels = {'sd RT (YA)'; 'sd RT (OA)'}; 

dataSelect = {'MRIRT_std'; 'MRIRT_std'};

h = figure('units','normalized','position',[.1 .1 .3 .25]);
set(0, 'DefaultFigureRenderer', 'painters'); hold on;
for indIdx = 1:2
    subplot(1,2,indIdx)
    cla; hold on;

    curData = STSWD_summary.behav.(dataSelect{indIdx});
    curData(curData==0) = NaN;
    if indIdx == 1
        curData = squeeze(curData(idx_YA,:));
    else 
        curData = squeeze(curData(idx_OA,:));
    end
    curData(isnan(curData(:,1)),:)=[]; % rm NaNs
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = squeeze(curData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData(:,i)-...
                nanmean(curData(:,:),2)+...
                repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(indIdx,:);

    box off
    cla;
    h_rc = rm_raincloud(data_ws, cl,1);
    view([90 -90]);
    axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel(paramLabels{indIdx})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim(lims{indIdx}); 
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-1*(curYTick(2)-curYTick(1)) curYTick(4)+1*(curYTick(2)-curYTick(1))]);
    
    IndividualSlopes_agree = [];
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes_agree(1,:) = b(2,:);
    [~, p(indIdx), ci, stats] = ttest(IndividualSlopes_agree(1,:));
    title(['linear:', num2str(round(p(indIdx), 3))])
end

set(findall(gcf,'-property','FontSize'),'FontSize',22)
% figureName = 'C_RT_ACC_RCP';
% saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
% saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
% saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% plot both groups simultaneously

colorm = [2.*[.3 .1 .1];  0.0314, 0.3176, 0.6118];

h = figure('units','normalized','position',[.1 .1 .2 .25]);
set(0, 'DefaultFigureRenderer', 'painters'); hold on;
data = []; data_ws = [];
for indIdx = 1:2
    curData = STSWD_summary.behav.(dataSelect{indIdx});
    if indIdx == 1
        curData = squeeze(curData(idx_YA,:));
        j = 1;
    else 
        curData = squeeze(curData(idx_OA,:));
        j = 2;
    end
    curData(isnan(curData(:,1)),:)=[]; % rm NaNs
    
 % read into cell array of the appropriate dimensions
    for i = 1:4
        data{i, j} = squeeze(curData(:,i));
        % individually demean for within-subject visualization
        data_ws{i, j} = curData(:,i)-...
            nanmean(curData(:,:),2)+...
            repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
    end
    
end

h_rc = rm_raincloud(data_ws, colorm,1); % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!
view([90 -90]);
axis ij
box(gca,'off')
set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
ylabel('Target load'); xlabel('SD RT');
set(findall(gcf,'-property','FontSize'),'FontSize',20)
xlim(lims{indIdx}); 
curYTick = get(gca, 'YTick'); ylim([curYTick(1)-1*(curYTick(2)-curYTick(1)) curYTick(4)+1*(curYTick(2)-curYTick(1))]);

set(findall(gcf,'-property','FontSize'),'FontSize',22)

figureName = 'a_sd-rt';
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');