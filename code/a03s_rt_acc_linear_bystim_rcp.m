% - plot attribute differences in magnitude of linear modulation by attribute mean(MR, EEG)

clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data         = fullfile(pn.root, 'data');
pn.bars         = fullfile(pn.root, 'tools', 'barwitherr'); addpath(pn.bars);
pn.brewer       = fullfile(pn.root, 'tools', 'BrewerMap'); addpath(pn.brewer);
pn.plotFolder   = fullfile(pn.root, 'figures');

load(fullfile(pn.data, 'SummaryData_N102.mat'), 'SummaryData', 'IDs_all');

%SummaryData.EEG.RTs_mean % ID*Attribute*Dimension

%% split results by young and old

IDs_group = str2num(cellfun(@(x)x(1), IDs_all));
idx_YA = IDs_group==1;
idx_OA = IDs_group==2;

attributes = {'Color','Direction','Size', 'Luminance'};

colorm = [230/265 25/265 25/265; 0/265 50/265 100/265]; % use external function to create colormap

for indFeature = 1:4
    data = squeeze(SummaryData.EEG.Acc_mean(:,indFeature,1:4));
    X = [1 1; 1 2; 1 3; 1 4]; b=X\data';
    LinearLoadEffects_Acc_EEG(:,indFeature) = b(2,:);
    data = squeeze(SummaryData.EEG.logRTs_mean(:,indFeature,1:4));
    X = [1 1; 1 2; 1 3; 1 4]; b=X\data';
    LinearLoadEffects_RT_EEG(:,indFeature) = b(2,:);
    
    data = squeeze(SummaryData.MRI.Acc_mean(:,indFeature,1:4));
    X = [1 1; 1 2; 1 3; 1 4]; b=X\data';
    LinearLoadEffects_Acc_MRI(:,indFeature) = b(2,:);
    data = squeeze(SummaryData.MRI.logRTs_mean(:,indFeature,1:4));
    X = [1 1; 1 2; 1 3; 1 4]; b=X\data';
    LinearLoadEffects_RT_MRI(:,indFeature) = b(2,:);
end

%% raincloud plot RT and Acc, merged across sessions

h = figure('units','normalized','position',[.1 .1 .4 .2]);

datMerged = squeeze(nanmean(cat(3, LinearLoadEffects_RT_EEG(idx_YA,:),LinearLoadEffects_RT_MRI(idx_YA,:)),3));

condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
condPairsLevel{1} = [.5 .51 .52 .53 .54, .55];
paramLabels{1} = 'linear beta RT';
indIdx = 1;
curData = datMerged;

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:4
    for j = 1:1
        data{i, j} = squeeze(curData(:,i));
        % individually demean for within-subject visualization
        data_ws{i, j} = curData(:,i)-...
            nanmean(curData(:,:),2)+...
            repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
    end
end

% IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

subplot(1,2,1)

cl = [.2 .6 .2];

box off
cla;
    h_rc = rm_raincloud(data, cl,1);
    % add stats
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05/size(condPairs,1) % Bonferroni correction
           mysigstar_vert(gca, [condPairsLevel{indIdx}(indPair), condPairsLevel{indIdx}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

        end
    end
    view([90 -90]);
    axis ij
box(gca,'off')
%set(gca, 'YTick', [1,2,3,4]);
set(gca, 'YTickLabels', {'Luminance'; 'Size'; 'Direction'; 'Color'});
ylabel('Feature'); xlabel(paramLabels{indIdx})
set(findall(gcf,'-property','FontSize'),'FontSize',20)
%xlim(lims{indIdx}); 
curYTick = get(gca, 'YTick'); ylim([curYTick(1)-1*(curYTick(2)-curYTick(1)) curYTick(4)+1*(curYTick(2)-curYTick(1))]);
xlim([0 .6])

% add accuracy

datMerged = squeeze(nanmean(cat(3, LinearLoadEffects_Acc_EEG(idx_YA,:),LinearLoadEffects_Acc_MRI(idx_YA,:)),3));

condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
condPairsLevel{1} = [.15 .17 .19 .21 .23];
condPairsLevel{2} = [-.2];
paramLabels{1} = 'linear beta accuracy';
indIdx = 1;
curData = datMerged;

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:4
    for j = 1:1
        data{i, j} = squeeze(curData(:,i));
        % individually demean for within-subject visualization
        data_ws{i, j} = curData(:,i)-...
            nanmean(curData(:,:),2)+...
            repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
    end
end

% IMPORTANT: plot individually centered estimates, stats on uncentered estimates!
subplot(1,2,2)
cl = [.2 .6 .2];

box off
cla;
    h_rc = rm_raincloud(data, cl,1);
    % add stats
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        critVal = (.05/size(condPairs,1));
        if pval < .05 % Bonferroni correction
           mysigstar_vert(gca, [condPairsLevel{indIdx}(indPair), condPairsLevel{indIdx}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);
        end
    end
    view([90 -90]);
    axis ij
box(gca,'off')
%set(gca, 'YTick', [1,2,3,4]);
set(gca, 'YTickLabels', {'Luminance'; 'Size'; 'Direction'; 'Color'});
ylabel('Feature'); xlabel(paramLabels{indIdx})
set(findall(gcf,'-property','FontSize'),'FontSize',20)
%xlim(lims{indIdx}); 
curYTick = get(gca, 'YTick'); ylim([curYTick(1)-1*(curYTick(2)-curYTick(1)) curYTick(4)+1*(curYTick(2)-curYTick(1))]);
xlim([-.2 .2])

figureName = 'E2_EEG_byAtt_RCP_supplement';

saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');
