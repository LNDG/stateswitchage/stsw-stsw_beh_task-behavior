## b_reliabilityAcrossSessions

*a01_prepareGroupAnalysis_StudyEEGMR_180309*

- extract RTs and ACCs from raw data, create matrices for EEG- and MR-based behavioral data

*a02_computeBehavioralSummaryStatistics*

- calculate Median (RT), Mean (Acc), STD summary statistics for data
- remove RT outliers > 3 STD from mean

*a03s_rt_acc_linear_bystim_plot*

- plot linear modulation by attribute (MR, EEG) vs. zero

![image](figures/E2_EEG_byAtt_supplement.png)

*a03s_rt_acc_linear_bystim_rcp*

- plot attribute differences in magnitude of linear modulation by attribute mean(MR, EEG)

![image](figures/E2_EEG_byAtt_RCP_supplement.png)

*a03s_rt_acc_vsGuess_OA*

![image](figures/E2_vsGuess_OA.png)

*a03s_rt_acc_vsGuess_YA*

![image](figures/E2_vsGuess_YA.png)

*b01_prepareOutputForR*

- run anova with mean RT and mean Acc

*c01_prepareOutputForR_linmixeffmod*

- run LME model with single trial RT and Acc

*c03_EffectsBySequencePosition*

- explore rt and acc effectrs related to within-block sequence position and time on task

![image](figures/c03_within-block.png)
![image](figures/c03_tot.png)

*d01_prepareDataForR*

- run LME model on single trials, includes probed attribute switch vs stay

2241 is an outlier for RTs in the EEG session (malfunctioning equipment? e.g. constant press)